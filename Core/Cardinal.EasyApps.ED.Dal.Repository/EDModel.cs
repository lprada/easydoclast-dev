﻿using System.Data.Entity;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.Dal.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class EDModel : DbContext
    {
        /// <summary>
        /// Gets or sets the lotes.
        /// </summary>
        /// <value>
        /// The lotes.
        /// </value>
        public DbSet<Lote> Lotes { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Entity<Lote>().Map(lote => new
        //                                    {
        //                                        cod_lote = lote.CodigoLote
        //                                    }
        //                                    ).ToTable("idlotes");
        //}
    }
}