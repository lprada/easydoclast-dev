﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Cliente
    /// </summary>
   [Serializable]
    public class Cliente
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cliente"/> class.
        /// </summary>
        
        public Cliente()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cliente"/> class.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <param name="nombre">The nombre.</param>
    
        public Cliente(string codigoCliente, string nombre)
        {
            CodigoCliente = codigoCliente;
            Nombre = nombre;
        }

        /// <summary>
        /// Gets or sets the codigo cliente.
        /// </summary>
        /// <value>
        /// The codigo cliente.
        /// </value>
        [Required(ErrorMessage = "Seleccione una empresa o sector.")]
        public string CodigoCliente { get; set; }

        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }
    }
}