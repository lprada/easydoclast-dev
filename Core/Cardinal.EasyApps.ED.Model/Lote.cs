﻿using System;
using System.Collections.Generic;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Lote
    /// </summary>
    public class Lote
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Lote"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lote()
        {
            Imagenes = new List<Imagen>();
        }

        /// <summary>
        /// Gets or sets the codigo lote.
        /// </summary>
        /// <value>
        /// The codigo lote.
        /// </value>
        public string CodigoLote { get; set; }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the fecha creacion.
        /// </summary>
        /// <value>
        /// The fecha creacion.
        /// </value>
        public DateTime? FechaCreacion { get; set; }

        /// <summary>
        /// Gets or sets the cantidad imagenes.
        /// </summary>
        /// <value>
        /// The cantidad imagenes.
        /// </value>
        public int? CantidadImagenes { get; set; }

        /// <summary>
        /// Gets or sets the plantilla.
        /// </summary>
        /// <value>
        /// The plantilla.
        /// </value>
        public Plantilla Plantilla { get; set; }



      

        /// <summary>
        /// Gets or sets the imagenes.
        /// </summary>
        /// <value>
        /// The imagenes.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Imagen> Imagenes { get; set; }
    }
}