﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Tipo de Campo
    /// </summary>
    public enum TipoCampo
    {
        /// <summary>
        /// Tipo Texto
        /// </summary>
        Texto = 12,
        /// <summary>
        /// Tipo Numero
        /// </summary>
        Numero = 3,
        /// <summary>
        /// Tipo Fecha
        /// </summary>
        Fecha = 9,
        /// <summary>
        /// Tipo Boleano de Verdadedo o Falso
        /// </summary>
        Boolean = 13,
       

    }
}
