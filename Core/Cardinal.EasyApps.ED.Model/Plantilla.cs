﻿using System.Collections.Generic;
using System.Linq;
using System;
namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Plantilla o Template
    /// </summary>
     [Serializable]
    public class Plantilla
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Plantilla"/> class.
        /// </summary>
        public Plantilla()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Plantilla"/> class.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <param name="nombre">The nombre.</param>
        /// <param name="cliente">The cliente.</param>
        public Plantilla(string codigoPlantilla, string nombre, Cliente cliente)
        {
            CodigoPlantilla = codigoPlantilla;
            Nombre = nombre;
            Cliente = cliente;
        }

        /// <summary>
        /// Gets or sets the codigo plantilla.
        /// </summary>
        /// <value>
        /// The codigo plantilla.
        /// </value>
        public string CodigoPlantilla { get; set; }
      

        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets the cliente.
        /// </summary>
        /// <value>
        /// The cliente.
        /// </value>
        public Cliente Cliente { get; set; }

        /// <summary>
        /// Gets or sets the volumenes validos.
        /// </summary>
        /// <value>
        /// The volumenes validos.
        /// </value>
        public IQueryable<Volumen> VolumenesValidos { get; set; }
    }
}