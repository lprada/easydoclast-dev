﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Volumen
    /// </summary>
    [Serializable]
    public class Volumen
    {

        private string _volumen;

        /// <summary>
        /// Gets or sets the volumen.
        /// </summary>
        /// <value>
        /// The volumen.
        /// </value>
        public string CodigoVolumen
        {
            get { return _volumen; }
            set { _volumen = value; }
        }



        /// <summary>
        /// Gets or sets the mount.
        /// </summary>
        /// <value>
        /// The mount.
        /// </value>
        public string Mount { get; set; }
        /// <summary>
        /// Gets or sets the mount acceso red.
        /// </summary>
        /// <value>
        /// The mount acceso red.
        /// </value>
        public string MountAccesoRed { get; set; }

        private DateTime? _fechaInicioValidez;

        /// <summary>
        /// Gets or sets the fecha inicio validez.
        /// </summary>
        /// <value>
        /// The fecha inicio validez.
        /// </value>
        public DateTime? FechaInicioValidez

        {
            get { return _fechaInicioValidez; }
            set { _fechaInicioValidez = value; }
        }

        private DateTime? _fechaFinValidez;

        /// <summary>
        /// Gets or sets the fecha fin validez.
        /// </summary>
        /// <value>
        /// The fecha fin validez.
        /// </value>
        public DateTime? FechaFinValidez
        {
            get { return _fechaFinValidez; }
            set { _fechaFinValidez = value; }
        }


    }
}
