﻿using System;
using System.Configuration;

namespace Cardinal.EasyApps.ED.Procesamiento
{
    /// <summary>
    ///
    /// </summary>
    public static class ProcesarImagen
    {
        /// <summary>
        /// Procesars the una imagen.
        /// </summary>
        /// <param name="pathImagen">The path imagen.</param>
        /// <param name="lote">The lote.</param>
        /// <param name="nroOrden">The nro orden.</param>
        /// <param name="indice">The indice.</param>
        public static void ProcesarUnaImagen(string pathImagen, Model.Lote lote, int nroOrden, int indice)
        {
            FormularioProcesamiento form = new FormularioProcesamiento();
            form.LoadImage(pathImagen);
            bool mostrarForm = Convert.ToBoolean(ConfigurationManager.AppSettings["MostarFormularioProcesamiento"]);
            if (mostrarForm)
            {
                form.Show();
            }
            form.ProcesarImagen(pathImagen, lote, nroOrden, indice);
            form.SaveImage(pathImagen);
            if (mostrarForm)
            {
                form.Hide();
            }
        }
    }
}