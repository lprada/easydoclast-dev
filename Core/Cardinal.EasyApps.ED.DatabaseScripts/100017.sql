/*
   Wednesday, July 23, 20089:42:44 AM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes ADD
	tag varchar(255) NULL
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per 