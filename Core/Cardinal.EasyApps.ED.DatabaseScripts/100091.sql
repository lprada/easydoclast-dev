/*
   Monday, April 25, 201111:37:38 AM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Cajas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.LotesCajas ADD CONSTRAINT
	FK_LotesCajas_Cajas FOREIGN KEY
	(
	cod_caja
	) REFERENCES dbo.Cajas
	(
	cod_caja
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.LotesCajas ADD CONSTRAINT
	FK_LotesCajas_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.LotesCajas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.LotesCajas', 'Object', 'CONTROL') as Contr_Per 
update Version
set DBVersion = '100091'
where Product = 'EasyDoc'
GO

select * from Version
where Product = 'EasyDoc'
GO