INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (296
           ,'en_us'     
           ,'User Type')
GO
INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (296
           ,'es_AR'     
           ,'Tipo Usuario')
GO

INSERT INTO MLObjects_T
           ([id]
           ,[MLMessage_ID])
     VALUES
           ('wfrmBusqUsers.uwgResultado.tipousuario'
           ,296)
GO

INSERT INTO MLObjects_T
           ([id]
           ,[MLMessage_ID])
     VALUES
           ('wfrmUserData.lblUserType'
           ,296)
GO

INSERT INTO MLObjects_T
           ([id]
           ,[MLMessage_ID])
     VALUES
           ('wfrmUserAdd.lblEmail'
           ,295)
GO

INSERT INTO MLObjects_T
           ([id]
           ,[MLMessage_ID])
     VALUES
           ('wfrmUserAdd.lblUserType'
           ,296)
GO


update Version set DBVersion ='100126' where Product='EasyDoc'


