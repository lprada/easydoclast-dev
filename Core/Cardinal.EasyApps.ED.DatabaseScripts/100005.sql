/*
   Friday, July 11, 20085:37:35 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SecuenciaLotes ADD CONSTRAINT
	FK_SecuenciaLotes_idlotes FOREIGN KEY
	(
	LoteInicioSecuencia
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.SecuenciaLotes ADD CONSTRAINT
	FK_SecuenciaLotes_idlotes1 FOREIGN KEY
	(
	LoteFinSecuencia
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'CONTROL') as Contr_Per 