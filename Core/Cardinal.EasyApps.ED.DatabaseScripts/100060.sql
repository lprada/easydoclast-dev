PRINT N'Dropping fk_LotesCajas_Cajas...';


GO
ALTER TABLE [dbo].[LotesCajas] DROP CONSTRAINT [fk_LotesCajas_Cajas];


GO
PRINT N'Dropping fk_LotesCajas_idlotes...';


GO
ALTER TABLE [dbo].[LotesCajas] DROP CONSTRAINT [fk_LotesCajas_idlotes];


GO
PRINT N'Dropping fk_montaje_vols...';


GO
ALTER TABLE [dbo].[montaje] DROP CONSTRAINT [fk_montaje_vols];


GO
PRINT N'Dropping fk_papelera_idlotes...';


GO
ALTER TABLE [dbo].[papelera] DROP CONSTRAINT [fk_papelera_idlotes];


GO
PRINT N'Dropping fk_papelera_tmplates...';


GO
ALTER TABLE [dbo].[papelera] DROP CONSTRAINT [fk_papelera_tmplates];


GO
PRINT N'Dropping pk_montaje...';


GO
ALTER TABLE [dbo].[montaje] DROP CONSTRAINT [pk_montaje];


GO
PRINT N'Dropping pk_vols...';


GO
ALTER TABLE [dbo].[vols] DROP CONSTRAINT [pk_vols];


GO

GO
PRINT N'Altering [dbo].[aud_capt]...';


GO
ALTER TABLE [dbo].[aud_capt] ALTER COLUMN [cod_lote] NVARCHAR (50) NOT NULL;


GO
PRINT N'Altering [dbo].[CajasDefinitivas]...';


GO
ALTER TABLE [dbo].[CajasDefinitivas] ALTER COLUMN [cod_caja] VARCHAR (30) NOT NULL;


GO
PRINT N'Creating PK_CajasDefinitivas...';


GO
ALTER TABLE [dbo].[CajasDefinitivas]
    ADD CONSTRAINT [PK_CajasDefinitivas] PRIMARY KEY CLUSTERED ([cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Altering [dbo].[grpyus]...';


GO
ALTER TABLE [dbo].[grpyus] ALTER COLUMN [login] VARCHAR (40) NOT NULL;


GO
PRINT N'Altering [dbo].[LotesCajas]...';


GO
ALTER TABLE [dbo].[LotesCajas] ALTER COLUMN [cod_caja] VARCHAR (30) NOT NULL;

ALTER TABLE [dbo].[LotesCajas] ALTER COLUMN [cod_lote] VARCHAR (50) NOT NULL;


GO
PRINT N'Creating [dbo].[LotesCajas].[IX_LotesCajas]...';


GO
CREATE CLUSTERED INDEX [IX_LotesCajas]
    ON [dbo].[LotesCajas]([cod_lote] ASC, [cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);


GO
PRINT N'Altering [dbo].[LotesCajasDefinitivas]...';


GO
ALTER TABLE [dbo].[LotesCajasDefinitivas] ALTER COLUMN [cod_caja] VARCHAR (30) NOT NULL;

ALTER TABLE [dbo].[LotesCajasDefinitivas] ALTER COLUMN [cod_lote] VARCHAR (50) NOT NULL;


GO
PRINT N'Creating PK_LotesCajasDefinitivas...';


GO
ALTER TABLE [dbo].[LotesCajasDefinitivas]
    ADD CONSTRAINT [PK_LotesCajasDefinitivas] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Altering [dbo].[montaje]...';


GO
ALTER TABLE [dbo].[montaje] ALTER COLUMN [volumen] VARCHAR (30) NOT NULL;


GO
PRINT N'Creating pk_montaje...';


GO
ALTER TABLE [dbo].[montaje]
    ADD CONSTRAINT [pk_montaje] PRIMARY KEY CLUSTERED ([volumen] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Altering [dbo].[vols]...';


GO
ALTER TABLE [dbo].[vols] ALTER COLUMN [volumen] VARCHAR (30) NOT NULL;


GO
PRINT N'Creating pk_vols...';


GO
ALTER TABLE [dbo].[vols]
    ADD CONSTRAINT [pk_vols] PRIMARY KEY CLUSTERED ([volumen] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Procesamientos]...';


GO
CREATE TABLE [dbo].[Procesamientos] (
    [cod_lote]             VARCHAR (50)  NOT NULL,
    [nro_orden]            SMALLINT      NOT NULL,
    [accion_procesamiento] VARCHAR (50)  NOT NULL,
    [cantidad]             INT           NOT NULL,
    [parametros]           VARCHAR (255) NOT NULL
);


GO
PRINT N'Creating PK_Procesamientos...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [PK_Procesamientos] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC, [accion_procesamiento] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[RZonaUsuario]...';


GO
CREATE TABLE [dbo].[RZonaUsuario] (
    [tmplate] VARCHAR (8)   NOT NULL,
    [orden]   SMALLINT      NOT NULL,
    [usuario] VARCHAR (200) NOT NULL,
    [tp]      INT           NOT NULL,
    [lft]     INT           NOT NULL,
    [bttm]    INT           NOT NULL,
    [rght]    INT           NOT NULL
);


GO
PRINT N'Creating PK_RZonaUsuario...';


GO
ALTER TABLE [dbo].[RZonaUsuario]
    ADD CONSTRAINT [PK_RZonaUsuario] PRIMARY KEY CLUSTERED ([tmplate] ASC, [orden] ASC, [usuario] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating PK_invalidos...';


GO
ALTER TABLE [dbo].[invalidos]
    ADD CONSTRAINT [PK_invalidos] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating PK_lotes...';


GO
ALTER TABLE [dbo].[lotes]
    ADD CONSTRAINT [PK_lotes] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[lotes].[IX_lotes_codlote_nroorden]...';


GO
CREATE NONCLUSTERED INDEX [IX_lotes_codlote_nroorden]
    ON [dbo].[lotes]([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);


GO
PRINT N'Creating PK_papelera...';


GO
ALTER TABLE [dbo].[papelera]
    ADD CONSTRAINT [PK_papelera] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC, [tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[SubLotes].[IX_sublotes_cod_lote_indice]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_sublotes_cod_lote_indice]
    ON [dbo].[SubLotes]([cod_lote] ASC, [indice] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);


GO
PRINT N'Creating DF_Procesamientos_cantidad...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [DF_Procesamientos_cantidad] DEFAULT ((0)) FOR [cantidad];


GO
PRINT N'Creating DF_Procesamientos_parametros...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [DF_Procesamientos_parametros] DEFAULT ('') FOR [parametros];


GO
PRINT N'Creating fk_montaje_vols...';


GO
ALTER TABLE [dbo].[montaje] WITH NOCHECK
    ADD CONSTRAINT [fk_montaje_vols] FOREIGN KEY ([volumen]) REFERENCES [dbo].[vols] ([volumen]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating [dbo].[listTableRowCounts]...';


GO
CREATE PROCEDURE [dbo].[listTableRowCounts] 
AS 
BEGIN 
    SET NOCOUNT ON 
 
    DECLARE @SQL VARCHAR(255) 
    SET @SQL = 'DBCC UPDATEUSAGE (' + DB_NAME() + ')' 
    EXEC(@SQL) 
 
    CREATE TABLE #foo 
    ( 
        tablename VARCHAR(255), 
        rc INT 
    ) 
     
    INSERT #foo 
        EXEC sp_msForEachTable 
            'SELECT PARSENAME(''?'', 1), 
            COUNT(*) FROM ?' 
 
    SELECT tablename, rc 
        FROM #foo 
        ORDER BY rc DESC 
 
    DROP TABLE #foo 
END
GO
PRINT N'Checking existing data aginst newly created constraints';


GO
ALTER TABLE [dbo].[montaje] WITH CHECK CHECK CONSTRAINT [fk_montaje_vols];


GO


update Version
set DBVersion = '100060'
where Product = 'EasyDoc'
GO

select * from Version
where Product = 'EasyDoc'
GO