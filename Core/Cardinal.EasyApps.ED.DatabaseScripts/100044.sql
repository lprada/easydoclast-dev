INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (260
           ,'es_AR'     
           ,'B')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (260
           ,'en_us'     
           ,'B')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (261
           ,'es_AR'     
           ,'D')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (261
           ,'en_us'     
           ,'D')
GO


INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqUsers.uwgResultado.bloq'
           ,260)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqUsers.uwgResultado.del'
           ,261)
GO

update Version
set DBVersion = '100044'
where Product = 'EasyDoc'
GO

select * from Version
where Product = 'EasyDoc'
GO