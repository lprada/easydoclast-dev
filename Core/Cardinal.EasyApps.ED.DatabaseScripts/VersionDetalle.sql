/****** Object:  Table [dbo].[VersionDetalles]    Script Date: 07/23/2008 10:09:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VersionDetalles](
	[VersionDetallesId] [uniqueidentifier] NOT NULL,
	[Product] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DBVersion] [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Fecha] [datetime] NOT NULL CONSTRAINT [DF_VersionDetalles_Fecha]  DEFAULT (getdate()),
	[Usuario] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ScriptName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Script] [image] NULL,
	[Modificacion] [timestamp] NOT NULL,
 CONSTRAINT [PK_VersionDetalles] PRIMARY KEY CLUSTERED 
(
	[VersionDetallesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF