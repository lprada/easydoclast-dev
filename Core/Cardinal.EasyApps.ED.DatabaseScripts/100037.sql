/*
   Tuesday, December 16, 20083:17:48 PM
   User: sa
   Server: csds-svr
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.aud_clas ADD
	TipoIndexacion int NOT NULL CONSTRAINT DF_aud_clas_TipoIndexacion DEFAULT 0
GO
COMMIT
select Has_Perms_By_Name(N'dbo.aud_clas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.aud_clas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.aud_clas', 'Object', 'CONTROL') as Contr_Per 