/*
   Monday, November 23, 20093:23:32 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Notificacion ADD
	Leida bit NOT NULL CONSTRAINT DF_Notificacion_Leida DEFAULT 'False',
	Archivada bit NOT NULL CONSTRAINT DF_Notificacion_Archivada DEFAULT 'False',
	FechaLectura datetime NULL,
	FechaArchivo datetime NULL
GO
ALTER TABLE dbo.Notificacion
	DROP COLUMN Estado
GO
ALTER TABLE dbo.Notificacion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Notificacion', 'Object', 'CONTROL') as Contr_Per 