/*
   Thursday, July 31, 20084:14:26 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.TmplatesDatosComerciales
	(
	Tmplate nvarchar(50) NOT NULL,
	BillingType nvarchar(50) NOT NULL,
	DiasProcesoWarning int NOT NULL,
	DiasProcesoError int NOT NULL,
	CajasMensuales int NOT NULL,
	SectorId uniqueidentifier NOT NULL,
	ResponsableId uniqueidentifier NOT NULL,
	PeriodicidadDias tinyint NOT NULL,
	VolumenEsperadoDiario bigint NOT NULL,
	VolumenEsperadoSemanal bigint NOT NULL,
	VolumenEsperadoMensual bigint NOT NULL,
	Instructivo nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_BillingType DEFAULT 'IMG' FOR BillingType
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_DiasProcesoWarning DEFAULT 0 FOR DiasProcesoWarning
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_DiasProcesoError DEFAULT 0 FOR DiasProcesoError
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_CajasMensuales DEFAULT 0 FOR CajasMensuales
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_PeriodicidadDias DEFAULT 0 FOR PeriodicidadDias
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	DF_TmplatesDatosComerciales_VolumenEsperadoMensual DEFAULT 00 FOR VolumenEsperadoMensual
GO
ALTER TABLE dbo.TmplatesDatosComerciales ADD CONSTRAINT
	PK_TmplatesDatosComerciales PRIMARY KEY CLUSTERED 
	(
	Tmplate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesDatosComerciales', 'Object', 'CONTROL') as Contr_Per 