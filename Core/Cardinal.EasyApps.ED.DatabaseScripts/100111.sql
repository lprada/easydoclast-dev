/*
   Tuesday, June 14, 20116:02:15 PM
   User: sa
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE Caratulas.Tmp_TB_DOCUMENTO
	(
	CD_CODIGO_DOCUMENTO bigint NOT NULL,
	CD_CAMPO varchar(255) NOT NULL,
	CD_PLANTILLA varchar(255) NOT NULL,
	VL_CAMPO varchar(255) NULL,
	TX_CAMPO varchar(255) NULL,
	TX_PLANTILLA varchar(255) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE Caratulas.Tmp_TB_DOCUMENTO SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM Caratulas.TB_DOCUMENTO)
	 EXEC('INSERT INTO Caratulas.Tmp_TB_DOCUMENTO (CD_CODIGO_DOCUMENTO, CD_CAMPO, CD_PLANTILLA, VL_CAMPO, TX_CAMPO, TX_PLANTILLA)
		SELECT CONVERT(bigint, CD_CODIGO_DOCUMENTO), CD_CAMPO, CD_PLANTILLA, VL_CAMPO, TX_CAMPO, TX_PLANTILLA FROM Caratulas.TB_DOCUMENTO WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE Caratulas.TB_DOCUMENTO
GO
EXECUTE sp_rename N'Caratulas.Tmp_TB_DOCUMENTO', N'TB_DOCUMENTO', 'OBJECT' 
GO
ALTER TABLE Caratulas.TB_DOCUMENTO ADD CONSTRAINT
	PK_TB_DOCUMENTO PRIMARY KEY CLUSTERED 
	(
	CD_CODIGO_DOCUMENTO
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'CONTROL') as Contr_Per 