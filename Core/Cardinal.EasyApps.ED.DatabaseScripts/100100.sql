/*
   Tuesday, May 31, 20112:17:28 PM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ActividadUsuario
	(
	ActividadUsuarioId bigint NOT NULL IDENTITY (1, 1),
	user_id char(50) NOT NULL,
	TareaUsuarioId bigint NOT NULL,
	FechaInicio datetime NOT NULL,
	FechaFin datetime NULL,
	cod_caja varchar(30) NULL,
	cod_lote varchar(50) NULL,
	cod_entrega varchar(30) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ActividadUsuario SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_ActividadUsuario OFF
GO
IF EXISTS(SELECT * FROM dbo.ActividadUsuario)
	 EXEC('INSERT INTO dbo.Tmp_ActividadUsuario (user_id, TareaUsuarioId, FechaInicio, FechaFin, cod_caja, cod_lote, cod_entrega)
		SELECT user_id, TareaUsuarioId, FechaInicio, FechaFin, cod_caja, cod_lote, cod_entrega FROM dbo.ActividadUsuario WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.ActividadUsuario
GO
EXECUTE sp_rename N'dbo.Tmp_ActividadUsuario', N'ActividadUsuario', 'OBJECT' 
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	PK_ActividadUsuario PRIMARY KEY CLUSTERED 
	(
	ActividadUsuarioId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'CONTROL') as Contr_Per 