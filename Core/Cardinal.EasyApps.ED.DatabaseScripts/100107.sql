
CREATE TABLE Caratulas.[TB_DOCUMENTO](
	[CD_CODIGO_DOCUMENTO] [numeric](18, 0) NOT NULL,
	[CD_CAMPO] [varchar](255) NOT NULL,
	[CD_PLANTILLA] [varchar](255) NOT NULL,
	[VL_CAMPO] [varchar](255) NULL,
	[TX_CAMPO] [varchar](255) NULL,
	[TX_PLANTILLA] [varchar](255) NULL
) ON [PRIMARY]

GO



CREATE TABLE Caratulas.[TB_EMPRESA_DOCUMENTOS](
	[CD_EMPRESA] [varchar](255) NOT NULL,
	[CD_CODIGO_DOCUMENTO] [numeric](18, 0) NOT NULL,
	[CD_USUARIO_CARGA] [varchar](100) NOT NULL,
	[FL_ESTADO_DOCUMENTO] [char](1) NOT NULL,
	[FC_CREACION] [datetime] NOT NULL,
	[CD_CODIGO_3D] [varchar](4296) NULL,
	[TX_EMPRESA] [varchar](255) NULL
) ON [PRIMARY]

GO



/****** Object:  Table [dbo].[TB_LOGS]    Script Date: 06/14/2011 17:20:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE Caratulas.[TB_LOGS](
	[CD_ID_LOG] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FC_MOVIMIENTO] [datetime] NULL,
	[CD_USUARIO_MOVIMIENTO] [varchar](50) NULL,
	[CD_MOVIMIENTO] [int] NULL,
	[TX_OBSERVACION] [varchar](400) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

