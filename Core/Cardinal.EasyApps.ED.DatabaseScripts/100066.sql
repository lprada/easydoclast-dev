
INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (275
           ,'en_us'     
           ,'Execute')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (275
           ,'es_AR'     
           ,'Ejecutar')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lnkEjecutar '
           ,275)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (276
           ,'en_us'     
           ,'EasyDoc Service')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (276
           ,'es_AR'     
           ,'Servicio de EasyDoc')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmPanelControl.EasyDocService '
           ,276)
GO



INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (277
           ,'en_us'     
           ,'Start')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (277
           ,'es_AR'     
           ,'Iniciar')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lnkIniciar '
           ,277)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (278
           ,'en_us'     
           ,'Stop')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (278
           ,'es_AR'     
           ,'Detener')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lnkDetener '
           ,278)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (279
           ,'en_us'     
           ,'ReStart')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (279
           ,'es_AR'     
           ,'ReIniciar')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lnkReiniciar '
           ,279)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (280
           ,'en_us'     
           ,'Service State:')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (280
           ,'es_AR'     
           ,'Estado del Servicio:')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lblEstadoServicioLabel '
           ,280)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (281
           ,'en_us'     
           ,'Service not Found')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (281
           ,'es_AR'     
           ,'Servicio no Encontrado')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lblEstadoNoEncontrado '
           ,281)
GO


INSERT INTO task_T
           (task_id
           ,descr
           ,lastNumber_bi)
     VALUES
           ('PCADMIN'
           ,'Tareas Administrativas del Tablero de Control'
           ,0)
GO

INSERT INTO task_T
           (task_id
           ,descr
           ,lastNumber_bi)
     VALUES
           ('SUSCRIP'
           ,'Suscripciones'
           ,0)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (282
           ,'en_us'     
           ,'Suscriptions')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (282
           ,'es_AR'     
           ,'Suscripciones')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmPanelControl.hplSuscripciones '
           ,282)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmSuscripciones.lblTitulo'
           ,282)
GO
