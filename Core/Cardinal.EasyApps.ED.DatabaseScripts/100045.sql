/*
   Tuesday, September 15, 200911:36:59 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MLMessages_T
	DROP CONSTRAINT fk_MLMessages_T_MLLanguages_T
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MLLanguages_T', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MLLanguages_T', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MLLanguages_T', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MLMessages_T
	(
	id int NOT NULL,
	MLLanguage_ID char(10) NOT NULL,
	text varchar(255) NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.MLMessages_T)
	 EXEC('INSERT INTO dbo.Tmp_MLMessages_T (id, MLLanguage_ID, text)
		SELECT CONVERT(int, id), MLLanguage_ID, text FROM dbo.MLMessages_T WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.MLMessages_T
GO
EXECUTE sp_rename N'dbo.Tmp_MLMessages_T', N'MLMessages_T', 'OBJECT' 
GO
ALTER TABLE dbo.MLMessages_T ADD CONSTRAINT
	pk_MLMessages_T PRIMARY KEY CLUSTERED 
	(
	id,
	MLLanguage_ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.MLMessages_T ADD CONSTRAINT
	fk_MLMessages_T_MLLanguages_T FOREIGN KEY
	(
	MLLanguage_ID
	) REFERENCES dbo.MLLanguages_T
	(
	id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.MLMessages_T', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MLMessages_T', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MLMessages_T', 'Object', 'CONTROL') as Contr_Per 