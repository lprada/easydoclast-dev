/*
   Wednesday, May 04, 20119:37:08 AM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.papelera
	DROP CONSTRAINT FK_papelera_idlotes
GO
ALTER TABLE dbo.idlotes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.papelera
	DROP CONSTRAINT FK_papelera_papelera
GO
ALTER TABLE dbo.tmplates SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_papelera
	(
	tmplate varchar(8) NOT NULL,
	cod_lote varchar(50) NOT NULL,
	nro_orden int NOT NULL,
	FechaHora datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_papelera SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.papelera)
	 EXEC('INSERT INTO dbo.Tmp_papelera (tmplate, cod_lote, nro_orden, FechaHora)
		SELECT tmplate, cod_lote, nro_orden, FechaHora FROM dbo.papelera WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.papelera
GO
EXECUTE sp_rename N'dbo.Tmp_papelera', N'papelera', 'OBJECT' 
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	PK_papelera_1 PRIMARY KEY CLUSTERED 
	(
	cod_lote,
	nro_orden
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	FK_papelera_papelera FOREIGN KEY
	(
	tmplate
	) REFERENCES dbo.tmplates
	(
	tmplate
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	FK_papelera_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.papelera', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'CONTROL') as Contr_Per 