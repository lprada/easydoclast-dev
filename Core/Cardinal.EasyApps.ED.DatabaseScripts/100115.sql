/*
   Friday, August 05, 201111:29:36 AM
   User: sa
   Server: (local)
   Database: EasyDoc.Caratulas
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ZCARATAB ADD
	Procesado bit NOT NULL CONSTRAINT DF_ZCARATAB_Procesado DEFAULT 'False'
GO
ALTER TABLE dbo.ZCARATAB SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
