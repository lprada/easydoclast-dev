/*
   Thursday, March 31, 20119:31:49 AM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_papelera
	(
	cod_lote varchar(50) NOT NULL,
	nro_orden int NOT NULL,
	tmplate varchar(8) NOT NULL,
	FechaHora datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_papelera SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.papelera)
	 EXEC('INSERT INTO dbo.Tmp_papelera (cod_lote, nro_orden, tmplate, FechaHora)
		SELECT cod_lote, nro_orden, CONVERT(varchar(8), tmplate), FechaHora FROM dbo.papelera WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.papelera
GO
EXECUTE sp_rename N'dbo.Tmp_papelera', N'papelera', 'OBJECT' 
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	PK_papelera_1 PRIMARY KEY CLUSTERED 
	(
	cod_lote,
	nro_orden
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	FK_papelera_papelera FOREIGN KEY
	(
	cod_lote,
	nro_orden
	) REFERENCES dbo.papelera
	(
	cod_lote,
	nro_orden
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.papelera', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'CONTROL') as Contr_Per 