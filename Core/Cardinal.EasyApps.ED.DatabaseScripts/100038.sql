/*
   Tuesday, June 02, 20099:39:27 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.OperacionEntregas
	(
	LoginEmpresa varchar(20) NOT NULL,
	OperacionId varchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.OperacionEntregas ADD CONSTRAINT
	PK_OperacionEntregas PRIMARY KEY CLUSTERED 
	(
	LoginEmpresa,
	OperacionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OperacionEntregas', 'Object', 'CONTROL') as Contr_Per 