﻿/*-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE DataosFacturacionHP
	-- Add the parameters for the stored procedure here
	@desde datetime , 
	@hasta datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select 'HP',
(select fechabajada from entregas where cod_entrega in 
(select cod_entrega from cajasentregas where cod_caja in 
(select cod_caja from lotescajas where cod_lote = z.cod_lote)))
,lote,count(*) as img,
(select count(distinct codryder + inirem) from ZLNDFWAB z1
where z1.cod_lote = z.cod_lote and z1.lote = z.lote 
) as doc

 from ZLNDFWAB z
left join papelera p on z.cod_lote = p.cod_lote and z.nro_orden = p.nro_orden 
where p.cod_lote is null
and z.cod_lote in (
select cod_lote from lotescajas where cod_caja in (
select cod_caja from cajasentregas where cod_entrega in (
select cod_entrega from entregas where (loginempresa ='RYDER'  or loginempresa ='DHLHP') and 
fechabajada >=@desde and  fechabajada <=@hasta)))
group by lote,z.cod_lote
order by 1,2
END*/
