﻿create function valorNumerico(@cadena varchar(50))
returns int
as
begin
  declare @cadaux varchar(50)
  declare @i int
  set @i = 1
  set @cadaux = ''
  while (@i <= len(@cadena))
  begin
    if (isnumeric(substring(@cadena,@i,1)) = 1) and (substring(@cadena,@i,1) <> '-') and (substring(@cadena,@i,1) <> '+')
    begin
      set @cadaux = @cadaux + substring(@cadena,@i,1)
    end
  set @i = @i + 1
  end
  return cast(@cadaux as int)
end

