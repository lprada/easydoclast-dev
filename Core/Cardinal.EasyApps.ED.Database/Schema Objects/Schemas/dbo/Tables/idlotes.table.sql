﻿CREATE TABLE [dbo].[idlotes] (
    [cod_lote]           VARCHAR (50)  NOT NULL,
    [tmplate]            VARCHAR (8)   NOT NULL,
    [act_nro]            SMALLINT      NULL,
    [ult_nro]            SMALLINT      NULL,
    [indexando]          SMALLINT      NULL,
    [caja]               INT           NULL,
    [feccreacion]        DATETIME      NULL,
    [scanner]            VARCHAR (20)  NULL,
    [controlrealizado]   SMALLINT      NULL,
    [tag]                VARCHAR (255) NULL,
    [usrindexando]       VARCHAR (255) NULL,
    [ipindexando]        VARCHAR (255) NULL,
    [enIndexacion]       VARCHAR (50)  NULL,
    [fechaUltAcceso]     DATETIME      NULL,
    [fechaProcDocumento] DATETIME      NULL
);



