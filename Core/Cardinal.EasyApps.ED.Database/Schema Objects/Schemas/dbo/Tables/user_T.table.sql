﻿CREATE TABLE [dbo].[user_T] (
    [user_id]                CHAR (50)     NOT NULL,
    [nameComplete]           VARCHAR (200) NOT NULL,
    [descr]                  VARCHAR (200) NULL,
    [changePassword]         CHAR (1)      NULL,
    [notAllowChangePassword] CHAR (1)      NULL,
    [bloq]                   CHAR (1)      NULL,
    [del]                    CHAR (1)      NULL,
    [password]               CHAR (100)    NOT NULL,
    [networkAlias]           VARCHAR (200) NULL,
    [fechalogin]             DATETIME      NULL,
    [fechapassword]          DATETIME      NULL,
    [reintetoslogin]         INT           NULL,
    [email]                  VARCHAR (200) NULL
);



