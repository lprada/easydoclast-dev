﻿CREATE TABLE [dbo].[Entregas] (
    [cod_entrega]    VARCHAR (30) NOT NULL,
    [LoginEmpresa]   VARCHAR (20) NULL,
    [nro_entrega]    INT          NULL,
    [Fecha]          DATETIME     NULL,
    [Estado]         INT          NULL,
    [CantImagenes]   INT          NULL,
    [CantDocumentos] INT          NULL,
    [CantCDs]        INT          NULL,
    [FechaBajada]    DATETIME     NULL
);



