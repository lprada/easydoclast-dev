﻿CREATE TABLE [dbo].[DocumentoSuscripcion] (
    [DocumentoSuscripcionId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]              DATETIME         NOT NULL,
    [Template]               VARCHAR (8)      NOT NULL,
    [UsuarioId]              CHAR (50)        NULL,
    [GrupoId]                CHAR (50)        NULL
);

