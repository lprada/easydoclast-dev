﻿CREATE TABLE [dbo].[aud_clas] (
    [cod_lote]       VARCHAR (50)  NOT NULL,
    [cod_us]         INT           NOT NULL,
    [fecha]          SMALLDATETIME NULL,
    [inicio]         SMALLDATETIME NOT NULL,
    [tiempo]         SMALLDATETIME NOT NULL,
    [totalim]        SMALLINT      NOT NULL,
    [clasif]         SMALLINT      NOT NULL,
    [deleted]        SMALLINT      NOT NULL,
    [depurar]        SMALLINT      NOT NULL,
    [caracter]       INT           NOT NULL,
    [documentos]     SMALLINT      NOT NULL,
    [TipoIndexacion] INT           NOT NULL
);



