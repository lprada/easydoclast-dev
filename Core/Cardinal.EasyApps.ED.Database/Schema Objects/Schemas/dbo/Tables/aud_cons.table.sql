﻿CREATE TABLE [dbo].[aud_cons] (
    [cod_lote] VARCHAR (50)  NOT NULL,
    [cod_us]   VARCHAR (50)  NOT NULL,
    [fecha]    SMALLDATETIME NOT NULL,
    [hora]     VARCHAR (200) NOT NULL,
    [orden]    INT           NOT NULL,
    [tmplate]  VARCHAR (8)   NOT NULL,
    [identDoc] VARCHAR (255) NULL
);



