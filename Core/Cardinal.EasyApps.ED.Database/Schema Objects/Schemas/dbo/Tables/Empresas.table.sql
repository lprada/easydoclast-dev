﻿CREATE TABLE [dbo].[Empresas] (
    [LoginEmpresa] VARCHAR (20)  NOT NULL,
    [DescEmpresa]  VARCHAR (50)  NULL,
    [NroCli]       VARCHAR (10)  NULL,
    [CUIT]         VARCHAR (20)  NULL,
    [PerInicio]    DATETIME      NULL,
    [PerDuracion]  INT           NULL,
    [DomCalle]     VARCHAR (30)  NULL,
    [DomNumero]    VARCHAR (20)  NULL,
    [DomPiso]      VARCHAR (10)  NULL,
    [DomLocalidad] VARCHAR (30)  NULL,
    [DomProvincia] VARCHAR (30)  NULL,
    [DomCP]        VARCHAR (10)  NULL,
    [ConApellido]  VARCHAR (30)  NULL,
    [ConNombre]    VARCHAR (30)  NULL,
    [ConCargo]     VARCHAR (30)  NULL,
    [ConTelefono]  VARCHAR (30)  NULL,
    [ConInterno]   VARCHAR (10)  NULL,
    [ConFax]       VARCHAR (30)  NULL,
    [ConEmail]     VARCHAR (100) NULL,
    [Estado]       VARCHAR (1)   NULL
);



