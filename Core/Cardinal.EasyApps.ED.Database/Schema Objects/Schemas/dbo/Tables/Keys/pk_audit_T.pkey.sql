﻿ALTER TABLE [dbo].[audit_T]
    ADD CONSTRAINT [pk_audit_T] PRIMARY KEY CLUSTERED ([fecha_dt] ASC, [user_id_fk] ASC, [task_id_fk] ASC, [task_number_bi] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

