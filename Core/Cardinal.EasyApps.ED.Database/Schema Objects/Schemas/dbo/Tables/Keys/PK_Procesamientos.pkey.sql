﻿ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [PK_Procesamientos] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC, [accion_procesamiento] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

