﻿ALTER TABLE [dbo].[tipos]
    ADD CONSTRAINT [pk_tipos] PRIMARY KEY CLUSTERED ([tipo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

