﻿ALTER TABLE [dbo].[montaje]
    ADD CONSTRAINT [pk_montaje] PRIMARY KEY CLUSTERED ([volumen] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

