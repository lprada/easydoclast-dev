﻿ALTER TABLE [dbo].[visores]
    ADD CONSTRAINT [pk_visores] PRIMARY KEY CLUSTERED ([visor] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

