﻿CREATE TABLE [dbo].[grpyus] (
    [codigo]       VARCHAR (200) NOT NULL,
    [tipo]         VARCHAR (1)   NULL,
    [descripcio]   VARCHAR (50)  NOT NULL,
    [aplicacion]   VARCHAR (40)  NULL,
    [login]        VARCHAR (40)  NOT NULL,
    [clave]        VARCHAR (50)  NULL,
    [LoginEmpresa] VARCHAR (20)  NULL,
    [Apellido]     VARCHAR (30)  NULL,
    [Nombre]       VARCHAR (30)  NULL,
    [Cargo]        VARCHAR (30)  NULL,
    [Telefono]     VARCHAR (30)  NULL,
    [Interno]      VARCHAR (10)  NULL,
    [Fax]          VARCHAR (30)  NULL,
    [Email]        VARCHAR (100) NULL,
    [CodSucursal]  INT           NULL,
    [TipoConexion] INT           NULL,
    [Estado]       VARCHAR (1)   NULL,
    [CambiarClave] VARCHAR (1)   NULL
);



