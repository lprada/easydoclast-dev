﻿CREATE TABLE [dbo].[LD_auditoriaDocumentos_T] (
    [fecha]          DATETIME      NULL,
    [caja]           VARCHAR (50)  NULL,
    [doc]            VARCHAR (50)  NULL,
    [lote]           VARCHAR (50)  NULL,
    [usuario]        VARCHAR (50)  NULL,
    [estOriCaja]     INT           NULL,
    [estDestCaja]    INT           NULL,
    [estOriDoc]      INT           NULL,
    [estDestDoc]     INT           NULL,
    [oper]           VARCHAR (50)  NULL,
    [numoper]        BIGINT        NULL,
    [comen]          VARCHAR (400) NULL,
    [estOriCajaDef]  INT           NULL,
    [estDestCajaDef] INT           NULL
);



