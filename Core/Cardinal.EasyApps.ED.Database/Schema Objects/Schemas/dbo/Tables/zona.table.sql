﻿CREATE TABLE [dbo].[zona] (
    [tmplate]      VARCHAR (8)   NOT NULL,
    [campo]        VARCHAR (8)   NOT NULL,
    [orden]        SMALLINT      NOT NULL,
    [tp]           INT           NULL,
    [lft]          INT           NULL,
    [bttm]         INT           NULL,
    [rght]         INT           NULL,
    [mndtory]      SMALLINT      NULL,
    [ocr1]         CHAR (1)      NULL,
    [ocr2]         CHAR (1)      NULL,
    [ocr3]         CHAR (1)      NULL,
    [ocr4]         SMALLINT      NULL,
    [ocr5]         SMALLINT      NOT NULL,
    [reftab]       VARCHAR (20)  NULL,
    [campoext]     VARCHAR (50)  NULL,
    [datasrc]      SMALLINT      NULL,
    [visual]       VARCHAR (5)   NULL,
    [filtrotabext] VARCHAR (255) NULL
);



