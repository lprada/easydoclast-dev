﻿CREATE TABLE [dbo].[rescanning_T] (
    [cod_lote]      VARCHAR (50)  NOT NULL,
    [nro_orden]     SMALLINT      NOT NULL,
    [marca]         TINYINT       NULL,
    [fechamarca]    DATETIME      NULL,
    [user_id]       CHAR (50)     NULL,
    [comentario]    VARCHAR (255) NULL,
    [clear]         CHAR (1)      NULL,
    [user_id_clear] CHAR (50)     NULL,
    [resultado]     VARCHAR (50)  NULL
);



