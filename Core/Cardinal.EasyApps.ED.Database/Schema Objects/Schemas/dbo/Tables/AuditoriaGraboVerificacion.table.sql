﻿CREATE TABLE [dbo].[AuditoriaGraboVerificacion] (
    [cod_us]    INT           NOT NULL,
    [fecha]     DATETIME      NOT NULL,
    [tmplate]   VARCHAR (8)   NOT NULL,
    [cod_lote]  VARCHAR (50)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [campo]     VARCHAR (8)   NOT NULL,
    [valor1]    VARCHAR (200) NOT NULL,
    [valor2]    VARCHAR (200) NOT NULL,
    [eleccion]  INT           NOT NULL
);



