﻿CREATE TABLE [dbo].[aud_capt] (
    [cod_lote] NVARCHAR (50) NOT NULL,
    [cod_us]   NVARCHAR (50) NOT NULL,
    [fecha]    SMALLDATETIME NOT NULL,
    [inicio]   SMALLDATETIME NOT NULL,
    [tiempo]   SMALLDATETIME NOT NULL,
    [total_im] SMALLINT      NOT NULL,
    [tipocap]  NVARCHAR (50) NOT NULL,
    [scanner]  NVARCHAR (20) NULL
);



