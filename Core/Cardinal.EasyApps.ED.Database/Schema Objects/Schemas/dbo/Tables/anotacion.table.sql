﻿CREATE TABLE [dbo].[anotacion] (
    [volumen]   VARCHAR (8)    NULL,
    [codlote]   VARCHAR (50)   NOT NULL,
    [nroorden]  SMALLINT       NOT NULL,
    [tipo]      VARCHAR (1)    NOT NULL,
    [txt]       VARCHAR (1024) NULL,
    [tp]        INT            NOT NULL,
    [lft]       INT            NOT NULL,
    [hght]      INT            NOT NULL,
    [wdth]      INT            NOT NULL,
    [backcolor] VARCHAR (12)   NULL,
    [tag]       INT            NULL
);



