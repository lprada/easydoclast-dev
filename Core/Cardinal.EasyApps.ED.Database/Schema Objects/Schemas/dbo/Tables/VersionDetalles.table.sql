﻿CREATE TABLE [dbo].[VersionDetalles] (
    [VersionDetallesId] UNIQUEIDENTIFIER NOT NULL,
    [Product]           NVARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DBVersion]         CHAR (6)         COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Fecha]             DATETIME         NOT NULL,
    [Usuario]           NVARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ScriptName]        NVARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Script]            IMAGE            NULL,
    [Modificacion]      TIMESTAMP        NOT NULL
);



