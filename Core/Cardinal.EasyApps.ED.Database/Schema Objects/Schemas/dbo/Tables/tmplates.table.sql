﻿CREATE TABLE [dbo].[tmplates] (
    [tmplate]             VARCHAR (8)   NOT NULL,
    [descripcio]          VARCHAR (50)  NOT NULL,
    [tipoindex]           SMALLINT      NOT NULL,
    [periodica]           SMALLINT      NOT NULL,
    [nro_zonas]           SMALLINT      NOT NULL,
    [tipo]                VARCHAR (2)   NOT NULL,
    [parent]              VARCHAR (8)   NOT NULL,
    [datasrc]             INT           NOT NULL,
    [ocr]                 SMALLINT      NOT NULL,
    [produccion]          VARCHAR (1)   NULL,
    [LoginEmpresa]        VARCHAR (20)  NULL,
    [BillingType]         VARCHAR (50)  NULL,
    [usaMergeDatos]       INT           NULL,
    [diasprocesoWarning]  INT           NULL,
    [diasprocesoError]    INT           NULL,
    [firmadigi]           CHAR (1)      NULL,
    [SeteosCaptura]       VARCHAR (255) NULL,
    [FormatoCapturaBN]    VARCHAR (255) NULL,
    [FormatoCapturaGrey]  VARCHAR (255) NULL,
    [FormatoCapturaColor] VARCHAR (255) NULL,
    [orderImagenes]       CHAR (1)      NULL,
    [cajasMensuales]      INT           NULL,
    [SeteosIndexacion]    VARCHAR (50)  NULL,
    [seteoDocumental]     TINYINT       NOT NULL
);



