﻿CREATE TABLE [dbo].[Documento] (
    [DocumentoId]   UNIQUEIDENTIFIER NOT NULL,
    [Nombre]        NVARCHAR (100)   NOT NULL,
    [FechaCreacion] DATETIME         NOT NULL
);

