﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Cardinal.EasyApps.ED.Common.Utils
{
    /// <summary>
    /// Clase para Validar CUIT
    /// </summary>
    public static class CUIT
    {
        /// <summary>
        /// Validars the cuit.
        /// </summary>
        /// <param name="cuit">The cuit.</param>
        /// <returns></returns>
        public static bool ValidarCuit(string cuit)
        {
            //      Dim Sum As Integer
            //Dim Coef As Variant
            //Dim i, Resto, Resultado
            //Coef = Array(0, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2)
            //Sum = 0
            //For i = 1 To 10
            //    Sum = Sum + Val(Mid(CUIT, i, 1)) * Coef(i)
            //Next
            //Resto = Sum Mod 11
            //Resultado = 11 - Resto
            //If Resultado = 11 Then
            //    Resultado = 0
            //ElseIf Resultado = 10 Then
            //    Resultado = 9
            //End If
            //DigiCuit = Resultado
            Regex rg = new Regex("[A-Z_a-z]");
            cuit = cuit.Replace("-", "");
            if (rg.IsMatch(cuit))
                return false;
            if (cuit.Length != 11)
                return false;
            int sum;
            int[] coef = new int[] { 0, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            int Resto;
            int Resultado;
            int digi = 0;
            sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += Convert.ToInt32(cuit.Substring(i, 1)) * coef[i + 1];
            }

            Resto = sum % 11;
            Resultado = 11 - Resto;
            if (Resultado == 11)
            {
                Resultado = 0;
            }
            else if (Resultado == 10)
            {
                Resultado = 9;
            }
            digi = Resultado;

            return digi.Equals(Convert.ToInt32(cuit.Substring(10, 1)));
        }
    }
}