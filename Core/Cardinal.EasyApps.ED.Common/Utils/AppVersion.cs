﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Cardinal.EasyApps.ED.Common.Utils
{
    /// <summary>
    /// Clase con funcionalidades sobre la Version del Aplicativo
    /// </summary>
   public static class AppVersion
    {
       /// <summary>
       /// Obtener la Version de la Aplicacion
       /// </summary>
       /// <returns></returns>
       [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
       public static string GetAppVersion()
       {
           System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

           var version = assembly.GetName().Version;

           return string.Format(CultureInfo.InvariantCulture,"{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
       }
    }
}
