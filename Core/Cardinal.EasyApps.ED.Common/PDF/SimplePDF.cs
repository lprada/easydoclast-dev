﻿using System;
using System.IO;
using iTextSharp.text.pdf;

namespace Cardinal.EasyApps.ED.Common.PDF
{
    /// <summary>
    /// 
    /// </summary>
    public class myTextDocument : iTextSharp.text.Document
    {
        /// <summary>
        /// Opens this instance.
        /// </summary>
        override public void Open()
        {
            // no hacemos nada
        }

        /// <summary>
        /// News the page.
        /// </summary>
        /// <returns></returns>
        override public bool NewPage()
        {
            // si el documento no está abierto, lo abrimos
            if (!this.IsOpen())
            {
                base.Open();
            }
            // agregamos la pagina
            base.NewPage();
            return true;
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        override public void Close()
        {
            // si estaba abierto, lo cerramos
            if (this.IsOpen())
            {
                base.Close();
            }
        }
    }

    /// <summary>
    /// Permisos PDF
    /// </summary>
    public class PDFPermissions
    {
        private string userPassword;
        private string ownerPassword;
        private int permissions;

        /// <summary>
        /// Initializes a new instance of the <see cref="PDFPermissions"/> class.
        /// </summary>
        public PDFPermissions()
        {
            userPassword = "";
            ownerPassword = "AlexiaFrancisco";
            this.AllowAssembly = false;
            this.AllowCopy = false;
            this.AllowDegradedPrinting = false;
            this.AllowFillIn = false;
            this.AllowModifyAnnotations = false;
            this.AllowModifyContents = false;
            this.AllowPrinting = true;
            this.AllowScreenReaders = false;
        }

        /// <summary>
        /// Gets the permissions as int.
        /// </summary>
        public int PermissionsAsInt
        {
            get { return permissions; }
        }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        /// <value>
        /// The user password.
        /// </value>
        public string UserPassword
        {
            get { return userPassword; }
            set { userPassword = value; }
        }

        /// <summary>
        /// Gets or sets the owner password.
        /// </summary>
        /// <value>
        /// The owner password.
        /// </value>
        public string OwnerPassword
        {
            get { return ownerPassword; }
            set { ownerPassword = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow assembly].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow assembly]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowAssembly
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowAssembly) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowAssembly;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowAssembly);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow copy].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow copy]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowCopy
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowCopy) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowCopy;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowCopy);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow degraded printing].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [allow degraded printing]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowDegradedPrinting
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowDegradedPrinting) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowDegradedPrinting;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowDegradedPrinting);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow fill in].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow fill in]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowFillIn
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowFillIn) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowFillIn;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowFillIn);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow modify annotations].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [allow modify annotations]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowModifyAnnotations
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowModifyAnnotations) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowModifyAnnotations;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowModifyAnnotations);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow modify contents].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow modify contents]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowModifyContents
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowModifyContents) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowModifyContents;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowModifyContents);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow printing].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow printing]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowPrinting
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowPrinting) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowPrinting;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowPrinting);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [allow screen readers].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow screen readers]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowScreenReaders
        {
            get { return (permissions | iTextSharp.text.pdf.PdfWriter.AllowScreenReaders) > 0; }
            set
            {
                if (value)
                {
                    permissions = permissions | iTextSharp.text.pdf.PdfWriter.AllowScreenReaders;
                }
                else
                {
                    permissions = permissions & (~iTextSharp.text.pdf.PdfWriter.AllowScreenReaders);
                }
            }
        }
    }

    /// <summary>
    /// Generador de PDF
    /// </summary>
    public class SimplePDF
    {
        iTextSharp.text.Document doc;
        iTextSharp.text.pdf.PdfWriter pdfW;
        Stream str = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimplePDF"/> class.
        /// </summary>
        public SimplePDF()
        {
        }

        /// <summary>
        /// Open_files the specified file_name.
        /// </summary>
        /// <param name="file_name">The file_name.</param>
        /// <param name="permisos">The permisos.</param>
        public void open_file(string file_name, PDFPermissions permisos)
        {
            doc = new myTextDocument();
            if (file_name.Equals(""))
            {
                str = new MemoryStream();
            }
            else
            {
                str = new FileStream(file_name, FileMode.Create);
            }
            pdfW = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, str);
            if (permisos != null)
            {
                pdfW.SetEncryption(iTextSharp.text.pdf.PdfWriter.STRENGTH128BITS, permisos.UserPassword, permisos.OwnerPassword, permisos.PermissionsAsInt);
            }
            doc.Open();
        }

        /// <summary>
        /// Open_files the specified file_name.
        /// </summary>
        /// <param name="file_name">The file_name.</param>
        public void open_file(string file_name)
        {
            open_file(file_name, null);
        }

        private void img_en_nueva_pagina(iTextSharp.text.Image img, int rotacion, string[] headers)
        {
            // si nos pasan la rotacion del EasyDoc (1,2,3), la convertimos
            if (rotacion == 1)
                rotacion = 270;
            if (rotacion == 2)
                rotacion = 180;
            if (rotacion == 3)
                rotacion = 90;
            int incremento = 0;
            int tamFuente = 0;
            float factor = 1;
            img.SetAbsolutePosition(0, 0);
            //img.ScaleAbsolute(doc.PageSize.Width,doc.PageSize.Height-100);
            //si los DPI horizontales y verticales son diferentes, lo compensamos
            if (img.DpiX != img.DpiY)
            {
                factor = (float)img.DpiX / (float)img.DpiY;
                img.ScaleAbsolute(img.Width, img.Height * factor);
            }
            img.RotationDegrees = rotacion;
            float ancho = img.Width;
            float alto = img.Height * factor;
            if ((rotacion == 90) || (rotacion == 270))
            {
                float aux = ancho;
                ancho = alto;
                alto = aux;
            }
            // calculamos el incremento en alto debido al header
            if (headers != null)
            {
                tamFuente = (int)((long)(alto * 40) / 3000);
                if (tamFuente < 12)
                    tamFuente = 12;
                incremento = (headers.Length * tamFuente) + 45;
            }
            iTextSharp.text.Rectangle tR = new iTextSharp.text.Rectangle(ancho, alto + incremento);
            doc.SetPageSize(tR);
            //doc.SetPageSize(iTextSharp.text.PageSize.LEGAL);
            //Console.WriteLine("width : " + img.Width + " - heigth : " + img.Height + " - fuente : " + tamFuente);
            doc.NewPage();
            pdfW.DirectContent.AddImage(img);
            // agregamos el header
            if (headers != null)
            {
                for (int i = 0; i < headers.Length; i++)
                {
                    iTextSharp.text.Paragraph pHead = new iTextSharp.text.Paragraph(tamFuente);
                    //iTextSharp.text.Phrase p = new iTextSharp.text.Phrase(headers[i],new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN,tamFuente));
                    iTextSharp.text.Phrase p = new iTextSharp.text.Phrase(headers[i], new iTextSharp.text.Font(iTextSharp.text.Font.NORMAL, tamFuente));
                    pHead.Add(p);
                    doc.Add(pHead);
                }
            }
        }

        /// <summary>
        /// Pdf_en_nueva_paginas the specified pdfBytes.
        /// </summary>
        /// <param name="pdfBytes">The pdfBytes.</param>
        /// <param name="rotacion">The rotacion.</param>
        public void pdf_en_nueva_pagina(string pdfBytes, int rotacion)
        {
            pdf_en_nueva_pagina(pdfBytes, rotacion, null);
        }


        /// <summary>
        /// Pdf_en_nueva_paginas the specified pdfBytes.
        /// </summary>
        /// <param name="pdfBytes">The pdfBytes.</param>
        /// <param name="rotacion">The rotacion.</param>
        public void pdf_en_nueva_pagina(byte[] pdfBytes, int rotacion)
        {
            pdf_en_nueva_pagina(pdfBytes, rotacion, null);
        }

        /// <summary>
        /// Pdf_en_nueva_paginas the specified pdfBytes.
        /// </summary>
        /// <param name="pdfBytes">The pdfBytes.</param>
        /// <param name="rotacion">The rotacion.</param>
        /// <param name="headers">The headers.</param>
        public void pdf_en_nueva_pagina(byte[] pdfBytes, int rotacion, string[] headers)
        {
            iTextSharp.text.pdf.PdfImportedPage page;

            using (Stream inputPdfStream = new MemoryStream(pdfBytes))
            {
                var reader = new PdfReader(inputPdfStream);
                //iTextSharp.text.pdf.RandomAccessFileOrArray ra = new iTextSharp.text.pdf.RandomAccessFileOrArray(pdfBytes);
                int paginas = reader.NumberOfPages;
                for (int i = 0; i < paginas; i++)
                {
                    //doc.SetPageSize(iTextSharp.text.PageSize.LEGAL);
                    doc.SetPageSize(reader.GetPageSize(i + 1));
                    doc.NewPage();
                    var cb = pdfW.DirectContent;
                    page = pdfW.GetImportedPage(reader, i + 1);

                    float origPageWidth = page.Width;
                    //float origPageHeight = reader.GetPageSize(i).Height;
                    //float newPageWidth = rect.Width;
                    //float newPageHeight = rect.Height;
                    float Scale = 0.80f;
                    if (rotacion == 90 || rotacion == 270 || origPageWidth > doc.PageSize.Width)
                    {
                        cb.AddTemplate(page, Scale, 0, 0, Scale, 0, 0);
                    }
                    else
                    {
                        cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0);
                    }
                }
            }
        }
        /// <summary>
        /// Pdf_en_nueva_paginas the specified pdfBytes.
        /// </summary>
        /// <param name="path_al_pdf">The pdfBytes.</param>
        /// <param name="rotacion">The rotacion.</param>
        /// <param name="headers">The headers.</param>
        public void pdf_en_nueva_pagina(string path_al_pdf, int rotacion, string[] headers)
        {
            iTextSharp.text.pdf.PdfImportedPage page;

            using (Stream inputPdfStream = new FileStream(path_al_pdf, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var reader = new PdfReader(inputPdfStream);
                //iTextSharp.text.pdf.RandomAccessFileOrArray ra = new iTextSharp.text.pdf.RandomAccessFileOrArray(pdfBytes);
                int paginas = reader.NumberOfPages;
                for (int i = 0; i < paginas; i++)
                {
                    //doc.SetPageSize(iTextSharp.text.PageSize.LEGAL);
                    doc.SetPageSize(reader.GetPageSize(i + 1));
                    doc.NewPage();
                    var cb = pdfW.DirectContent;
                    page = pdfW.GetImportedPage(reader, i + 1);

                    float origPageWidth = page.Width;
                    //float origPageHeight = reader.GetPageSize(i).Height;
                    //float newPageWidth = rect.Width;
                    //float newPageHeight = rect.Height;
                    float Scale = 0.80f;
                    if (rotacion == 90 || rotacion == 270 || origPageWidth > doc.PageSize.Width)
                    {
                        cb.AddTemplate(page, Scale, 0, 0, Scale, 0, 0);
                    }
                    else
                    {
                        cb.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0);
                    }
                }
            }
        }

        /// <summary>
        /// Tiff_en_nueva_paginas the specified path_al_tiff.
        /// </summary>
        /// <param name="path_al_tiff">The path_al_tiff.</param>
        /// <param name="rotacion">The rotacion.</param>
        /// <param name="headers">The headers.</param>
        public void tiff_en_nueva_pagina(string path_al_tiff, int rotacion, string[] headers)
        {
            iTextSharp.text.pdf.RandomAccessFileOrArray ra = new iTextSharp.text.pdf.RandomAccessFileOrArray(path_al_tiff);
            int paginas = iTextSharp.text.pdf.codec.TiffImage.GetNumberOfPages(ra);
            for (int i = 0; i < paginas; i++)
            {
                iTextSharp.text.Image img = iTextSharp.text.pdf.codec.TiffImage.GetTiffImage(ra, i + 1);
                img_en_nueva_pagina(img, rotacion, headers);
            }
            ra.Close();
        }

        /// <summary>
        /// Tiff_en_nueva_paginas the specified path_al_tiff.
        /// </summary>
        /// <param name="path_al_tiff">The path_al_tiff.</param>
        /// <param name="rotacion">The rotacion.</param>
        public void tiff_en_nueva_pagina(string path_al_tiff, int rotacion)
        {
            tiff_en_nueva_pagina(path_al_tiff, rotacion, null);
        }

        /// <summary>
        /// Jpg_en_nueva_paginas the specified path_al_jpg.
        /// </summary>
        /// <param name="path_al_jpg">The path_al_jpg.</param>
        /// <param name="rotacion">The rotacion.</param>
        /// <param name="headers">The headers.</param>
        public void jpg_en_nueva_pagina(string path_al_jpg, int rotacion, string[] headers)
        {
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(path_al_jpg);
            img_en_nueva_pagina(img, rotacion, headers);
        }

        /// <summary>
        /// Jpg_en_nueva_paginas the specified path_al_jpg.
        /// </summary>
        /// <param name="path_al_jpg">The path_al_jpg.</param>
        /// <param name="rotacion">The rotacion.</param>
        public void jpg_en_nueva_pagina(string path_al_jpg, int rotacion)
        {
            jpg_en_nueva_pagina(path_al_jpg, rotacion, null);
        }

        /// <summary>
        /// Get_file_byteses this instance.
        /// </summary>
        /// <returns></returns>
        public Byte[] get_file_bytes()
        {
            return ((MemoryStream)str).ToArray();
        }

        /// <summary>
        /// Close_files this instance.
        /// </summary>
        public void close_file()
        {
            doc.Close();
            str.Close();
        }
    }
}
