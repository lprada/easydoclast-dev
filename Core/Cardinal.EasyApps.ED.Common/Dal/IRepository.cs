﻿using System.Linq;
using Cardinal.EasyApps.ED.Model;
using System.Collections.Generic;
using System;

namespace Cardinal.EasyApps.ED.Common.Dal
{
    /// <summary>
    /// Interface para el Repositorio de ED.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets the DB version.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        string GetDBVersion();

        /// <summary>
        /// Gets the lotes por estado.
        /// </summary>
        /// <param name="estado">The estado.</param>
        /// <returns></returns>
        IQueryable<Lote> GetLotesPorEstado(EstadoLote estado);

        /// <summary>
        /// Gets the lotes A procesar Y transferir.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Lote> GetLotesAProcesarYTransferir();

        /// <summary>
        /// Gets the lotes con error transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<LoteConError> GetLotesConErrorTransferenciaYProcesamiento();

        /// <summary>
        /// Gets the lotes historial transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Lote> GetLotesHistorialTransferenciaYProcesamiento();

        /// <summary>
        /// Gets the imagenes lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <returns></returns>
        IQueryable<Imagen> GetImagenesLote(Lote lote);

        /// <summary>
        /// Gets the path red volumen.
        /// </summary>
        /// <param name="volumen">The volumen.</param>
        /// <returns></returns>
        string GetPathRedVolumen(string volumen);

        /// <summary>
        /// Gets the extension tipo.
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        /// <returns></returns>
        string GetExtensionTipo(int tipo);

        /// <summary>
        /// Poners the en papelera.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="nroOrden">The nro orden.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "En")]
        void PonerEnPapelera(Lote lote, int nroOrden);

        /// <summary>
        /// Marcars the como sub lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="indice">The indice.</param>
        /// <param name="patchCode">The patch code.</param>
        /// <param name="eliminarImagenAnterior">if set to <c>true</c> [eliminar imagen anterior].</param>
        void MarcarComoSubLote(Lote lote, int indice, Model.PatchCodeValues patchCode, bool eliminarImagenAnterior);

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Cliente> GetEmpresas();

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        IQueryable<Cliente> GetEmpresas(string loginToken);

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<PlantillaVolumen> GetPlantillasVolumenes();


        ///// <summary>
        ///// Gets the volumenes validos.
        ///// </summary>
        ///// <param name="CodigoPlantilla">The codigo plantilla.</param>
        ///// <returns></returns>
        //IQueryable<Volumen> GetVolumenesValidos(string CodigoPlantilla);

        /// <summary>
        /// Gets the proximo nombre lote.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="prefijoLote">The prefijo lote.</param>
        /// <param name="codigoEstacionEscaneoImportacion">The codigo estacion escaneo importacion.</param>
        /// <param name="mascaraNumeroLote">The mascara numero lote.</param>
        /// <returns></returns>
        string GetProximoNombreLote(string path, string prefijoLote, string codigoEstacionEscaneoImportacion, string mascaraNumeroLote);

        /// <summary>
        /// Agregars the lote importacion.
        /// </summary>
        /// <param name="loteAImportar">The lote A importar.</param>
        void AgregarLoteImportacion(LoteImportar loteAImportar);

      /// <summary>
        /// Existes the lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        bool ExisteLote(string codLote);

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <returns></returns>
        bool ExisteCaja(string codCaja);

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        bool ExisteCaja(string codCaja, string codigoPlantilla);

        /// <summary>
        /// Determines whether [is login token valid] [the specified login token].
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns>
        ///   <c>true</c> if [is login token valid] [the specified login token]; otherwise, <c>false</c>.
        /// </returns>
        bool IsLoginTokenValid(string loginToken);

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas();
        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas(string codigoCliente);


        /// <summary>
        /// Gets the plantilla campos.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        IQueryable<PlantillaCampo> GetPlantillaCampos(string codigoPlantilla);



        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillasWithLoginToken(string loginToken);
        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas(string loginToken,string codigoCliente);


        


        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        IQueryable<TablaExterna> GetTablasExternas();


        /// <summary>
        /// Gets the usuario.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        string GetUsuarioNombre(string loginToken);

        /// <summary>
        /// Gets the usuario id.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        Guid GetUsuarioId(string loginToken);


        /// <summary>
        /// Modificars the debe cambiar password.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        void ModificarDebeCambiarPassword(string loginToken, bool value);
        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        IQueryable<TablaExterna> GetTablasExternas(string loginToken);

        /// <summary>
        /// Gets the historico by usuario.
        /// </summary>
        /// <param name="nroUsuario">The nro usuario.</param>
        /// <returns></returns>
        IQueryable<HitoricoCS> GetHistoricoByUsuario(string nroUsuario);

        /// <summary>
        /// Anulars the caratula cliente.
        /// </summary>
        /// <param name="id_caratula">The id_caratula.</param>
        /// <returns></returns>
        bool AnularCaratulaCliente(int id_caratula);

        /// <summary>
        /// Gets the campos by documento.
        /// </summary>
        /// <param name="nroDocumento">The nro documento.</param>
        /// <returns></returns>
        IQueryable<CamposCS> GetCamposByDocumento(decimal nroDocumento);


        /// <summary>
        /// Logs the operation caratulas.
        /// </summary>
        /// <param name="logDTO">The log DTO.</param>
        void LogOperationCaratulas(LogCaratula logDTO);


        /// <summary>
        /// Gets the caratula vista por id.
        /// </summary>
        /// <param name="nroCaratula">The nro caratula.</param>
        /// <returns></returns>
        CaratulaCS GetCaratula(long nroCaratula);


        ///// <summary>
        ///// Gets the caratula completa.
        ///// </summary>
        ///// <returns></returns>
        //IQueryable<CaratulaCS> GetCaratulaCompleta();


        /// <summary>
        /// Gets the max document ID.
        /// </summary>
        /// <returns></returns>
        int GetMaxDocumentID();

        /// <summary>
        /// Saves the documento.
        /// </summary>
        /// <param name="documento">The documento.</param>
        /// <returns></returns>
        long saveDocumento(DocumentoEmpresa documento);


        /// <summary>
        /// Existes the key valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        bool ExisteKeyValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key);

        /// <summary>
        /// Agregars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        bool AgregarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string value);

        /// <summary>
        /// Editars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        bool EditarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string newValue);

        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulas();

        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulasPorLote(string codigoLote);
        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulas(string username);

        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <param name="empresa">The empresa.</param>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulasEmpresa(string empresa);



        /// <summary>
        /// Realizars the inbound.
        /// </summary>
        /// <param name="numeroCaratula">The numero caratula.</param>
        void RealizarInbound(long numeroCaratula);

        /// <summary>
        /// Realizars the outbound.
        /// </summary>
        /// <param name="numeroCaratula">The numero caratula.</param>
        void RealizarOutbound(long numeroCaratula);

        /// <summary>
        /// Gets the numero caratulas pendientes procesar.
        /// </summary>
        /// <returns></returns>
        List<KeyValuePair<string, Lote>> GetNumeroCaratulasPendientesProcesar();

        /// <summary>
        /// Gets the lote.
        /// </summary>
        /// <param name="cod_lote">The cod_lote.</param>
        /// <returns></returns>
        Model.Lote GetLote(string cod_lote);

        /// <summary>
        /// Gets the imagenes caratula.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <param name="codigoCaratula">The codigo caratula.</param>
        /// <returns></returns>
        IQueryable<Imagen> GetImagenesCaratula(string codLote, long codigoCaratula);



        /// <summary>
        /// Adds the lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="imagenes">The imagenes.</param>
        /// <param name="car">The car.</param>
        void AddLote(Lote lote, List<Imagen> imagenes,CaratulaCS car);



        /// <summary>
        /// Merges the lote.
        /// </summary>
        /// <param name="CodLote">The cod lote.</param>
        /// <param name="car">The car.</param>
        void MergeLote(string CodLote, CaratulaCS car);

        /// <summary>
        /// Gets the caratulas procesadas por lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulasProcesadasPorLote(string codLote);

        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <param name="caratulas">The caratulas.</param>
        /// <returns></returns>
        List<CaratulaCS> GetCaratulas(long[] caratulas);

        /// <summary>
        /// Saves the caratula.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        long SaveCaratula(CaratulaCS item);


        /// <summary>
        /// Get Instancia de Aplicacion
        /// </summary>
        /// <returns>DESA o PROD</returns>
        string GetInstancia();

        /// <summary>
        /// Gets the campo descripcion.
        /// </summary>
        /// <param name="plantillaId">The plantilla id.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoValue">The campo value.</param>
        /// <returns></returns>
        string GetCampoDescripcion(string plantillaId, string campoId, string campoValue);

        /// <summary>
        /// Delete caratulas
        /// </summary>
        /// <returns>sa</returns>
        void BorrarCaratulas();
    }
}