﻿*****************
Elmah for Glimpse
*****************
Thanks for using Elmah for Glimpse.

Project Homepage
----------------
http://elmah4glimpse.codeplex.com/

Using it
--------
To use Elmah for Glimpse, all you need to do is:
1. Install the package through NuGet or manually reference the Glimpse.Elmah.dll
2. Include the Elmah for Glimpse Javascript in all your pages: 
   <script src="<%: Url.Content("~/Glimpse/Resource/?resource=Pager") %>" type="text/javascript"></script>

Author
------
Steven Lauwers

Release Notes
-------------
06/05/2011 - 0.9.3.0
- Implement the IProvideGlimpseHelp interface
- Change default page size to 10
- Fix bug in pager script

06/03/2011 - 0.9.2.0
- Updated to version 0.82 of Glimpse
- Updated to version 1.2.0.1 of Elmah

05/31/2011 - 0.9.1.0
- Added details link
- Added pager

05/18/2011 - 0.9.0.0
- Initial version