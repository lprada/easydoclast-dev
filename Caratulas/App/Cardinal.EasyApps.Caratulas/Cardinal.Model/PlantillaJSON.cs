﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Cardinal.Model
{
    [Serializable]
    public class PlantillaJSON
    {
        public string CodigoPlantilla { get; set; }
        public string Nombre { get; set; }
        public ClienteJSON Cliente { get; set; }
        public IQueryable<VolumenJSON> VolumenesValidos { get; set; }
    }
}
