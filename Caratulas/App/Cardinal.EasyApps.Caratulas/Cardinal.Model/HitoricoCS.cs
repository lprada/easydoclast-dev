﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    public class HitoricoCS
    {
        public decimal codigo { get; set; }
        public DateTime fecha { get; set; }
        public string empresa { get; set; }
        public string plantilla { get; set; }
        public string nombre_empresa { get; set; }

        public string usuario { get; set; }
        public IList<CamposCS> campos_valores { get; set; }
    }
}
