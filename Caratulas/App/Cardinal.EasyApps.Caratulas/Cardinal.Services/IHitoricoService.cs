﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;

namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHitoricoService" in both code and config file together.
    [ServiceContract]
    public interface IHitoricoService
    {
        [OperationContract]
        List<HitoricoCS> GetHistoricoByUsuario(string nroUsuario);

        [OperationContract]
        bool AnularCaratulaCliente(int id_caratula);

        [OperationContract]
        List<CamposCS> GetCamposByDocumento(decimal nroDocumento);
    }
}
