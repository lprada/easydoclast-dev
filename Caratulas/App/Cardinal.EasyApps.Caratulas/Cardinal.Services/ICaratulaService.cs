﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;


namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICaratulaService" in both code and config file together.
    [ServiceContract]
    public interface ICaratulaService
    {
        [OperationContract]
        IList<CaratulaCS> GetCaratulaVistaPorId(int nroCaratula);

        [OperationContract]
        IList<CaratulaCS> GetCaratulaCompleta();

        [OperationContract]
        int GetMaxDocumentID();

    }
}
