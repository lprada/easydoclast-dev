﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Configuration;


namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AuthenticationService" in code, svc and config file together.
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Si el retorno es "OK" significa que la autenticación ha sido correcta | Si se devuelve "FAIL" significa que no se pudo autenticar</returns>
        public Token authenticateJSON(string username, string password)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["LoginServiceJSON"].ToString());

            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("username={0}&password={1}", username, password);
 
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);


            request.ContentLength = byteData.Length;


            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            Token token = new Token();
     
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    jsonResponse = reader.ReadToEnd();
                    JObject o = JObject.Parse(jsonResponse);
                    token.Result = (string)o["Result"];
                    token.TokenTicket = (string)o["Token"];
                    reader.Close();
                }
            #endregion
            
            return token;
        }
    }
}
