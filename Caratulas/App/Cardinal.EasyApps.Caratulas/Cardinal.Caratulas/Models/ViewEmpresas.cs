﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Cardinal.EasyApps.ED.Model;

namespace Cardinal.Caratulas.Models
{
    public class ViewEmpresas
    {
        public IEnumerable<Cliente> listaEmpresas { get; set; }
    }
}