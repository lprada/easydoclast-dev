﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Cardinal.EasyApps.ED.Model.Cliente>" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Alta de carátulas por empresa</h2>

<script src="../../Scripts/jquery.validate.js" type="text/javascript"></script>


<%--<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j("#Plantillas").attr('disabled', true)


    $j(function () {

        var urlemp = '<%: ViewBag.PlantillaPorEmpresaServiceJSONURL  %>';

        $j("#Empresas").change(function () {
            $j('#loading-pane').show();
            
            var categoriaId = $j(this).val();
            var textoValor= $j(this).find("option:selected").text();

            $j.ajax({
                type: 'GET',

                url: '<%: ViewBag.PlantillaPorEmpresaServiceJSONURL  %>',
                data: { valor: categoriaId, texto: textoValor },
                datatype: 'JSON',


                success: function (data) {
                    $j('#loading-pane').hide();
                   
                    if (data.length > 0) {
                        var options = '';
                        for (p in data) {
                            var subcategorias = data[p];
                            options += "<option value='" + subcategorias.CodigoPlantilla + "'>" + subcategorias.Nombre + "</option>";

                        }

                        $j("#Plantillas").removeAttr('disabled').html(options);
                        $j("#MainContent_CrearControles").attr('disabled', false)

                 


                    } else {

                        $j("#Plantillas").attr('disabled', true).html('');
                        $j("#MainContent_CrearControles").attr('disabled', true)

                    }
                }, //function,


                error: function (xhr, type, exception) {
                    window.alert(urlemp + "/" + categoriaId);

                    alert("Error: " + type);
                    alert("Error: " + exception);
                    alert("Error: " + xhr.status);

                }
            })


        });
    });

    $j(document).ready(function () {
        $j("#formCaratulas").validate();
    });


</script> --%>

<script type="text/javascript">
   

    function PlantillasCombo_OnDataBound(e) {
        var comboBox = $("#Plantillas").data("tComboBox");

        var index =0;
        comboBox.select(index);
    }
    function EmpresasCombo_onChange(e) {
        // your code
     
        //var comboboxEmp = $('#Empresas').data('tComboBox');
      
     
        $("#Plantillas").data("tComboBox").reload();
    }

</script>
  <style type="text/css">
        .HelperPlantillas
        {
            display:block;
            padding:10px;

  border:1px solid black;
 
  background-color:#EDE8EA; color:#696868;
  text-align: center; 
  font-family:Calibri;
  font-size: small;
        }
        .field-validation-error { color: red; position: absolute; margin: 0 0 0 5px; }
   .validation-summary-errors {border:1px solid #DFDFDF; background: #FDFDFD; color:Gray;}
   
   #loading-pane { display:none; opacity: 0.8;  position: fixed; top: 0; left: 0; 
    width: 100%; height: 100%; filter: alpha(opacity=80); -moz-opacity: 0.8; 
    z-index: 999998; background-color: #ffffff; }
#loading-pane img { position: absolute; top: 40%; left: 47%; z-index: 999999; }

      .style1
      {
          width: 4px;
      }

      .style2
      {
          width: 55px;
      }

      .style3
      {
          width: 191px;
      }

    </style>

    <div id="loading-pane">
     <img src="../../Content/ajax-loader.gif" />
    </div>

<%Html.EnableClientValidation(); %>
<%= Html.ValidationSummary() %>
<% using (Html.BeginForm("Create", "Caratula", FormMethod.Post, new { id = "formCaratulas", name = "formCaratulas" }))
 { %>
   <fieldset style="padding:5px;">
        <legend>Datos de carga</legend>
        <table style="width: 625px">
        <tr>
        <td class="style2">
        <div class="display-label">Empresa</div>
        </td>
        <td class="style3">
            <div class="editor-label">
           <%-- <%: Html.DropDownList("Empresas", String.Empty)%>--%>
              <%= Html.Telerik().ComboBox()
                     .Name("Empresas").SelectedIndex(0)
                          .AutoFill(true).HtmlAttributes(new { style = "width:500px" })
                                      .BindTo(new SelectList(ViewBag.Empresas, "CodigoCliente", "Nombre"))
                .Filterable(filtering =>
                {
                    filtering.FilterMode(AutoCompleteFilterMode.StartsWith);
                })
                .HighlightFirstMatch(true)
                     .ClientEvents(events => events
                      .OnChange("EmpresasCombo_onChange")
                                    
                                     
             )
        %>
            <%= Html.ValidationMessage("Error", "*")%>
            <%: Html.ValidationMessageFor(model => model.CodigoCliente) %>
        </div></td>

        <td class="style1" rowspan="2">
            <div style="width: 234px" id="HelperPlantillas" class="HelperPlantillas">
             Seleccione empresa y plantilla y luego presione <span style="color:#5772BD; font-weight:bold;">'Cargar Datos'</span>
            </div></td>
        </tr>
        <tr>
        <td class="style2">
       
        <div class="display-label">Plantilla</div>
       
        </td>
        <td class="style3">
         <div class="editor-label" >
        <%-- <%: Html.DropDownList("Plantillas",String.Empty)%>--%>

           <%= Html.Telerik().ComboBox()
                          .Name("Plantillas").HtmlAttributes(new { style = "width:500px" }).SelectedIndex(0)
                .AutoFill(true)
                     .DataBinding(binding => binding.Ajax().Select("Plantillas_AjaxLoading", "Caratula", new { codEmpresa = "TECO" }))
                .Filterable(filtering =>
                {
                    filtering.FilterMode(AutoCompleteFilterMode.StartsWith);
                })
                .HighlightFirstMatch(true).Enable(true)
                      .ClientEvents(events => events
                       
                                          .OnDataBound("PlantillasCombo_OnDataBound")

                  )
        %>
        </div>
        </td>
        </tr>
        </table>
       
        <br /><br />
        <%= ViewBag.Controles%>
        <input type="submit" id="CrearControles" runat="server" value="Cargar datos" />
    <%=ViewBag.Scripts %>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Limpiar formulario", "Index") %>
</div>

</asp:Content>
