﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Cardinal.Caratulas.Models.VistaCaratula>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VerHistorico
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% %>
<h2>Ver Historico</h2>
<% 
     var pagerStyleFlags = new[] 
    { 
        new { Key = "pageInput", Value = GridPagerStyles.PageInput },
        new { Key = "nextPrevious", Value = GridPagerStyles.NextPrevious },
        new { Key = "numeric", Value = GridPagerStyles.Numeric },
        new { Key = "pageSize", Value = GridPagerStyles.PageSizeDropDown }
    };
    GridPagerStyles pagerStyles = GridPagerStyles.NextPreviousAndNumeric;
    foreach (var pagerStyleFlag in pagerStyleFlags)
    {
        bool pagerStyle = (bool)ViewData[pagerStyleFlag.Key];
        if (pagerStyle == true)
        {
            pagerStyles |= pagerStyleFlag.Value;
        }
        else
        {
            pagerStyles &= ~pagerStyleFlag.Value;
        }
    }
    var position = (GridPagerPosition)ViewData["position"];
    var currentPage = (int)ViewData["currentPage"];
    
    currentPage = Math.Max(currentPage, 1);
    currentPage = Math.Min(currentPage, 83);

    
    
    Html.Telerik().Grid(Model)
       .Name("Grid")
       .Columns(columns =>
       {
           columns.Bound(o => o.CD_CODIGO_DOCUMENTO).Width(100);
           columns.Bound(o => o.FC_CREACION).Width(100);
           columns.Bound(o => o.CD_EMPRESA).Width(100);
           columns.Bound(o => o.desc_plantilla).Width(100);
           columns.Bound(o => o.CD_USUARIO_CARGA).Width(100);
       })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("_Paging", "Grid"))
        .Pageable(paging => paging.Style(pagerStyles).Position(position).PageTo(currentPage))

       .Render();
      
       
%>
</asp:Content>
