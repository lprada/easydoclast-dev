﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable< Cardinal.EasyApps.ED.Model.HitoricoCS>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Ver Historico
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% %>
<h2>Ver Historico</h2>
<% 
    Html.Telerik().Grid(Model)
       .Name("Grid")
       .DataKeys(keys => { keys.Add(o => o.codigo); })
       .Columns(columns =>
       {
           columns.Bound(o => o.codigo).Title("Documento").Format("{0:F0}");
           columns.Bound(o => o.fecha).Title("Fecha");
           columns.Bound(o => o.nombre_empresa).Title("Empresa").Format("{0:F0}");
           columns.Bound(o => o.plantilla).Title("Plantilla");
           columns.Bound(o => o.codigo).Width(50).Format(Html.ActionLink("Ver", "VerReporte", new { id = "{0}" }, new { @class = "botonGrid_2" }).ToHtmlString()).Encoded(false).Title("Ver Reporte").Width(10);
           columns.Bound(o => o.codigo).Width(50).Format(Html.ActionLink("Anular", "AnularCaratula", new { id = "{0}" }, new { @class = "botonGrid_2" }).ToHtmlString()).Encoded(false).Title("Anular").Width(10);
       })
       .DetailView(detailView => detailView.Template(e => 
       {
            %>
                <%= Html.Telerik().Grid(e.campos_valores)
                        .Name("Campos_" + e.codigo)
                        .Columns(columns =>
                        {
                            columns.Bound(o => o.campo).Title("Campo");
                            columns.Bound(o => o.valor).Title("Valor");
                        })
                        .Footer(false)
                %>
              <%
       }))
       .Pageable(paging => paging.PageSize(10))
       .Sortable()
       .Render(); 
%>
</asp:Content>
