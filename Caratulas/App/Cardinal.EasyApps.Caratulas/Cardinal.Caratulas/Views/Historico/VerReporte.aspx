﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=5.0.11.510, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<%@ Import  Namespace="MessagingToolkit.QRCode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Ver Reporte
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%string dir = Url.Action("GetPdfCaratula", "Caratula", new { caratulaId = ViewContext.RouteData.Values["id"].ToString() });%>
<%=Html.ActionLink("Volver", "VerHistorico", "Historico") %>
 &nbsp; -
<%= Html.ActionLink("Bajar Caratula", "GetPdfCaratula", "Caratula", new { caratulaId = ViewContext.RouteData.Values["id"].ToString() },null)%>
<br />
<br />
 <object data="<%= dir %>" width="100%" height="800px" type="application/pdf" />
</asp:Content>
