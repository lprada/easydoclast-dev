﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Collections;
using Telerik.Web.Mvc;
using System.Text;
using Telerik.Reporting.Processing;
using Cardinal.EasyApps.ED.Model;
using Cardinal.EasyApps.ED.Common.Dal;
using System.Configuration;

namespace Cardinal.Caratulas.Controllers
{
    public class plantilla
    {
        public decimal CD_PLANTILLA { get; set; }

        public string tx_Plantilla { get; set; }
    }
    [Authorize]
    public class CaratulaController : Controller
    {
        IRepository _repository;

        public CaratulaController(IRepository repository)
        {
            _repository = repository;
        }
       
        private List<SelectListItem> _products = new List<SelectListItem>();

        public ActionResult Index()
        {
            ViewBag.Plantillas = new SelectList(_products);
            ViewBag.Controles = "";

           // SelectList lista = new SelectList(, "CodigoCliente", "Nombre");
            ViewBag.Empresas = ConexionServiciosED.GetEmpresasJSON((Token)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;
           
            _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });
            ViewBag.Scripts = "<script>$j('#MainContent_CrearControles').attr('disabled', true);</script>";
            ViewBag.Scripts = "";


           // ViewBag.PlantillaPorEmpresaServiceJSONURL = ConfigurationManager.AppSettings["PlantillaPorEmpresaServiceJSON"].ToString();
            return View();
        }

        [HttpPost]
        public ActionResult Empresa_AjaxLoading(string text)
        {
            List<Cliente> empresasJSON = ConexionServiciosED.GetEmpresasJSON((Token)Session["TokenTicket"]);
            if (!String.IsNullOrWhiteSpace(text))
            {
                empresasJSON = empresasJSON.Where(p => p.Nombre.Contains(text)).ToList();
            }
         

            return new JsonResult
            {
                Data = new SelectList(empresasJSON, "CodigoCliente", "Nombre")
            };
        }

        [HttpPost]
        public ActionResult Plantillas_AjaxLoading(string text, string codEmpresa)
        {
            var emp = ConexionServiciosED.GetEmpresasJSON((Token)Session["TokenTicket"]).Where(p => p.CodigoCliente == codEmpresa).FirstOrDefault();
            Session["Empresa"] = emp;
            var query = ConexionServiciosED.GetPlantillasByEmpresaJSON((Token)Session["TokenTicket"], codEmpresa);
            Session["PlantillasJSON"] = query.ToList();
            List<Plantilla> plantillasJSON = ConexionServiciosED.GetPlantillasByEmpresaJSON((Token)Session["TokenTicket"],codEmpresa);
            if (!String.IsNullOrWhiteSpace(text))
            {
                plantillasJSON = plantillasJSON.Where(p =>  p.Nombre.Contains(text)).ToList();
            }
            SelectList lista = new SelectList(plantillasJSON, "CodigoPlantilla", "Nombre");

            return new JsonResult
            {
                Data = lista
            };
        }

        public ActionResult Create(FormCollection formulario)
        {
            SelectList lista = new SelectList(ConexionServiciosED.GetEmpresasJSON((Token)Session["TokenTicket"]), "CodigoCliente", "Nombre");
            ViewBag.Empresas = lista;
           
            var value = String.Empty;
            var valueEmpresas = String.Empty;
            StringBuilder sb = new StringBuilder();
            ViewBag.Controles = "";
            string status = "";
            
            if (formulario.Keys.Count > 2)
            {
                status = formulario[0];
            }

            if (status == "save")
            {
                if (formulario.Keys.Count != 0)
                {
                    #region Documento
                    int IDDocumentoMax = 0;
                    IDDocumentoMax = _repository.GetMaxDocumentID() + 1;
                   
                    #endregion

                    value = Session["Plantilla"].ToString();

                    valueEmpresas = ((Cliente)Session["Empresa"]).CodigoCliente;

                    var filteredList = from myClass in ((IList<Cliente>)Session["EmpresasJSON"]).ToList()
                                       where myClass.CodigoCliente == valueEmpresas

                                       select myClass.Nombre;
                    Cliente empresa = new Cliente
                    {
                        CodigoCliente = valueEmpresas,
                        Nombre = filteredList.ToList().First().ToString()                 
                    };
                    List<CamposCS> listaCampos = new List<CamposCS>();
                    DocumentoEmpresa documento = new DocumentoEmpresa
                    {
                        empresa = empresa,
                        estadoDocumento = "1",
                        fechaCreacion = DateTime.Now,
                        usuarioCarga = Session["UserName"].ToString(),
                        codigoDocumento = IDDocumentoMax

                    };

                    var filteredPlantilla = from plantillas in ((IList<Plantilla>)Session["PlantillasJSON"]).ToList()
                                            where plantillas.CodigoPlantilla == value

                                            select plantillas.Nombre;

                    for (int i = 1; i < formulario.Count - 1; i++)
                    {
                        string[] campoVector = formulario.Keys[i].ToString().Split('_');
                        CamposCS campo = new CamposCS { campo = campoVector[0].ToString(), valor = formulario[i], txCampo = campoVector[1].ToString() };
                        listaCampos.Add(campo);
                    }
                    documento.listaCampos = listaCampos;
                    documento.plantilla = value;
                    documento.txPlantilla = filteredPlantilla.ToList().First().ToString();
                    documento.codigo3d = generarValores3d(IDDocumentoMax, listaCampos);

                    #region GuardarDocumento
                    RessultMessage message = new RessultMessage();
                    message = _repository.saveDocumento(documento);

                    if (message.status == "OK")
                    {
                        //Session["Documento"] = documento.codigoDocumento;
                        _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se dió de alta el siguiente documento " + documento.codigo3d });
                        //return RedirectToAction("Index", "Message", new { message = "El documento fue creado con éxito" });
                        return RedirectToAction("VerReporte", "Historico", new { id = documento.codigoDocumento });
                    }
                    #endregion
                    
                    return View();
                }
                else
                {
                    ViewBag.Plantillas = new SelectList(_products);
                    ViewBag.Controles = "";
                    ViewBag.Scripts = "";
                }
            }
            else
            {
                foreach (var key in formulario.AllKeys)
                {
                    if (key == "Plantillas")
                    {
                        value = formulario[key];
                    }
                    if (key == "Empresas")
                    {
                        valueEmpresas = formulario[key];
                    }
                }

                var queryEmpresaSelected = ConexionServiciosED.GetEmpresasJSON((Token)Session["TokenTicket"]);
                ViewBag.Empresas = new SelectList(queryEmpresaSelected, "CodigoCliente", "Nombre", ((Cliente)Session["Empresa"]).CodigoCliente);

                var query = ConexionServiciosED.GetPlantillasByEmpresaJSON((Token)Session["TokenTicket"], ((Cliente)Session["Empresa"]).CodigoCliente);

                ViewBag.Plantillas = new SelectList(query, "CodigoPlantilla", "Nombre", value);
                if (((Cliente)Session["Empresa"]).CodigoCliente == "")
                {
                    ModelState.AddModelError("Error", "Seleccione una empresa");
                }

                #region CrearControlesDinamicos
                var queryCampos = ConexionServiciosED.GetCamposByPlantillaJSON((Token)Session["TokenTicket"], value);
                ViewBag.Controles = crearControles(queryCampos.ToList());
                Session["Plantilla"] = value;
                if (queryCampos.ToList().Count > 0)
                {
                    ViewBag.EnableSubmit = "disabled";
                    ViewBag.Scripts = "<script>$j('#Empresas').attr('disabled', true);</script><script>$j('#Plantillas').attr('disabled', true);</script><script>$j('#MainContent_CrearControles').attr('disabled', true);</script>";
                    //ViewBag.Scripts = "";
                }
                else
                {
                    ViewBag.EnableSubmit = "enabled";
                    ViewBag.Scripts = "<script>$j('#Empresas').attr('disabled', false);</script><script>$j('#Plantillas').attr('disabled', false);</script>";
                    //ViewBag.Scripts = "";
                }
                #endregion
            }
            
            return View();
        }

        private string generarValores3d(int IDDocumentoMax, List<CamposCS> listaCampos)
        {
            StringBuilder sb3d = new StringBuilder();
            sb3d.Append(IDDocumentoMax);
            sb3d.Append(",");
            foreach (CamposCS item in listaCampos)
            {
                sb3d.Append(item.valor);
                sb3d.Append(",");
            }
            return sb3d.ToString();
        }

        private dynamic crearControles(List<PlantillaCampo> list)
        {
            var sb = new StringBuilder().Append("<fieldset>");
            sb.AppendFormat("<legend>{0}</legend>", "Formulario");

            #region StatusSave
            sb.AppendFormat(
                "<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{3}\">",
                "status",
                "status",
                String.Empty,
                "save");
            #endregion
            sb.AppendFormat(
                "<table>");

            foreach (PlantillaCampo item in list)
            {
                switch (item.TipoCampo)
                {
                    case TipoCampo.Fecha:
                        GenerarHTMLFecha(sb, item);
                        break;
                    case TipoCampo.Texto:
                        GenerateHTMLCampoTexto(sb, item);
                        break;
                    case TipoCampo.Numero:
                       

                        GenerateHTMLCampoNumero(item, sb);
                       
                        break;
                    default:
                        break;
                }
            }
            
            sb.AppendFormat(
                "</table>");
            //sb.AppendFormat(
            //    "<div style='width: 234px' id='HelperCampos' class='HelperCampos'> Cargue los datos y luego presione <span style='color:#5772BD; font-weight:bold;'>'Generar Reporte'</span>  </div>"
            //    );
            sb.Append("</fieldset>");
            return sb.ToString();
        }
  
        private void GenerateHTMLCampoNumero(PlantillaCampo item, StringBuilder sb)
        {
            string esObligatorio = "";
            if (item.EsObligatorio)
            {
                esObligatorio = "required";
            }
            else
            {
                esObligatorio = "";
            }
            sb.AppendFormat(
                "<tr>");
            sb.AppendFormat(
                "<td><label for=\"{0}\">{1}:</label></td>" +
                "<td><input type=\"text\" id=\"{0}\" name=\"{0}\" class=\"{4}\" value=\"{2}\" MAXLENGTH=\"{3}\" data-val-required='Campo Requerido.'></td>",
                item.CampoId + "_" + item.Nombre.Replace(' ', '-'),
                item.Nombre,
                0,
                item.CantidadCaracteresMaximos,
                esObligatorio);

            sb.Append("<script>$j('#" + item.CampoId + "_" + item.Nombre.Replace(' ', '-') + "').keypress(");
            //                function(event) { if (event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 48) {if (!parseInt(String.fromCharCode(event.which))) { event.preventDefault();}}}
            sb.Append(" function validate(evt) {");
            sb.Append(" var theEvent = evt || window.event;");
            sb.Append(" var key = theEvent.keyCode || theEvent.which;");
            sb.Append(" key = String.fromCharCode( key );");
            sb.Append(@" var regex = /[0-9]|\./;");
            sb.Append(" if( !regex.test(key) ) {");
            sb.Append("   theEvent.returnValue = false;");
            sb.Append("   if(theEvent.preventDefault) theEvent.preventDefault();");
            sb.Append("}");
            sb.Append("}");

            sb.Append(");</script>");
            sb.AppendFormat(
                "</tr>");
        }

        private static void GenerarHTMLFecha(StringBuilder sb, PlantillaCampo item)
        {
            #region Campos Fecha

            sb.AppendFormat(
                "<tr>");
            sb.AppendFormat(
                "<td>");
            sb.AppendFormat(
                "<label for=\"{0}\">{1}:</label>",
                item.CampoId + "_" + item.Nombre,
                item.Nombre);
            sb.AppendFormat(
                "</td>");
            sb.AppendFormat(
                "<td>");

            string esObligatorioFecha = "";
            if (item.EsObligatorio)
            {
                esObligatorioFecha = "required";
            }
            else
            {
                esObligatorioFecha = "";
            }

            sb.AppendFormat(
                "<input type=\"text\" id=\"{0}\" name=\"{0}\"  class=\"{4}\" value=\"{2}\" MAXLENGTH=\"{3}\" data-val-required='The Idade field is required.'>",
                item.CampoId + "_" + item.Nombre.Replace(' ', '-'),
                item.Nombre,
                String.Empty,
                item.CantidadCaracteresMaximos,
                esObligatorioFecha);
            sb.Append(
                "<script>$j(function () {$j('#" + item.CampoId + "_" + item.Nombre.Replace(' ', '-') + "').datepicker( $j.datepicker.regional['es']);});</script>");
            sb.AppendFormat(
                "</td>");
            //sb.Append(
            //     "<script>$(function() {$('" + item.CampoId + "').datepicker();})</script>"
            //    );
            sb.AppendFormat(
                "</tr>");
            #endregion
        }

        private static void GenerateHTMLCampoTexto(StringBuilder sb, PlantillaCampo item)
        {
            string esObligatorioNumero = "";
            if (item.EsObligatorio)
            {
                esObligatorioNumero = "required";
            }
            else
            {
                esObligatorioNumero = "";
            }
            sb.AppendFormat(
                "<tr>");
            sb.AppendFormat(
                "<td><label for=\"{0}\">{1}:</label></td>", item.CampoId + "_" + item.Nombre, item.Nombre);
            sb.Append("<td>");
            if (item.ValoresPosibles.Count > 0)
            {
                //armo el select
                sb.AppendFormat("<select id=\"{0}\" name=\"{0}\" class=\"{4}\" data-val-required='El campo {1} es obligatorio.'>",
                                item.CampoId + "_" + item.Nombre,
                                item.Nombre,
                                String.Empty,
                                item.CantidadCaracteresMaximos,
                                esObligatorioNumero);
                // armo los options
                foreach (var valor in item.ValoresPosibles)
                {
                    sb.AppendFormat("<option value='{1}'>{0}</option>", valor.Value, valor.Key);
                }
                sb.Append("</select>");
            }
            else
            {
                sb.AppendFormat("<input type=\"text\" id=\"{0}\" name=\"{0}\" class=\"{4}\" value=\"{2}\" MAXLENGTH=\"{3}\" data-val-required='El campo {1} es obligatorio.'>",
                                item.CampoId + "_" + item.Nombre,
                                item.Nombre,
                                String.Empty,
                                item.CantidadCaracteresMaximos,
                                esObligatorioNumero);
            }
            sb.Append("</td>");
            sb.AppendFormat(
                "</tr>");
        }

        public string crearControles()
        {
            var sb = new StringBuilder().Append("<fieldset>");

            sb.AppendFormat("<legend>{0}</legend>", "Formulario");

            sb.AppendFormat(
                "<p><label for=\"{0}\">{1}:</label>" +
                "<input type=\"text\" id=\"{0}\" name=\"{0}\" value=\"{2}\"></p>",
                "Codigo Empresa",
                "cod_Empresa",
                22);

            sb.Append("<div style='width: 200px' id='HelperCampos' class='HelperCampos'> Cargue los datos y luego presione <span style='color:#5772BD; font-weight:bold;'>'Genera Reporte'</span>  </div>");
            sb.Append("</fieldset>");
            return sb.ToString();
        }

        public ActionResult GenerarControles(FormCollection formulario)
        {
            return View();
        }

        public JsonResult GetDocumentosForEmpresa(string valor, string texto)
        {
            //decimal idConv = int.Parse(valor);
            if (valor == "")
            {
                ModelState.AddModelError("Error", "Seleccione una empresa");
            }
            else
            {
                ModelState.Remove("Error");
            }
            //var query = servicioWCF.getPlantillasByEmpresa(Convert.ToInt16(idConv));
            var query = ConexionServiciosED.GetPlantillasByEmpresaJSON((Token)Session["TokenTicket"], valor);
            Session["PlantillasJSON"] = query.ToList();
            Session["Empresa"] = new Cliente { CodigoCliente = valor, Nombre = texto };
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPdfCaratula(int caratulaId)
        {
            Cardinal.Caratulas.Reportes.Report4 reporte = new Cardinal.Caratulas.Reportes.Report4();

            reporte.DataSource = _repository.GetCaratulaVistaPorId(caratulaId).ToArray();

            MessagingToolkit.QRCode.Codec.QRCodeEncoder qr = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            string str = ((CaratulaCS[])(reporte.DataSource))[0].codigo_3d;//(reporte.Items.Find("codigo_3d_not_visible", true)[0] as Telerik.Reporting.TextBox).Value;
            System.Drawing.Bitmap bmp = qr.Encode(str);
            (reporte.Items.Find("picBoxQR", true)[0] as Telerik.Reporting.PictureBox).Value = bmp;

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", reporte, null);
            return File(result.DocumentBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, String.Format("Caratula {0}.pdf", caratulaId));
        }
    }
}