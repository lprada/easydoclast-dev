﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Infraestructure
{
    public abstract class AccessComon
    {
        /// <summary>
        /// Api Url
        /// </summary>
        string _apiUrl;

        public string ApiUrl
        {
            get { return _apiUrl; }
            set { _apiUrl = value; }
        }
        /// <summary>
        /// Api Version
        /// </summary>
        string _apiVersion;

        public string ApiVersion
        {
            get { return _apiVersion; }
            set { _apiVersion = value; }
        }

        public string ApiUrlFull
        {
            get
            {
                return string.Format("{0}", _apiUrl, _apiVersion);
            }
        }

        public AccessComon(string apiUrl, string apiVersion)
        {
            _apiUrl = apiUrl;
            _apiVersion = apiVersion;
        }

        public AccessComon(string apiUrl) : this(apiUrl,string.Empty)
        {
        }

        public AccessComon(): this(string.Empty, string.Empty)
        {
        }


        protected string GetJsonResponse(HttpWebRequest request, string jsonResponse)
        {
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                reader.Close();
            }
            return jsonResponse;
        }


        protected HttpWebRequest ResquestPost(Uri address, string data)
        {
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            return request;
        }

    }
}