﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Infraestructure
{
    public class ConexionDirectaED :  Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        public Cardinal.EasyApps.ED.Common.Dal.IRepository Repository
        {
            get { return _repository; }
            set { _repository = value; }
        }
        public ConexionDirectaED(Cardinal.EasyApps.ED.Common.Dal.IRepository repository)
        {
            _repository = repository;
            
        }
        public ConexionDirectaED()
        {
           

        }

        public bool AddValorTablaExterna(Model.LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string value)
        {

            if (!ValidoToken(token))
            {
                return false;
            }
       

         
            if (!_repository.ExisteKeyValorTablaExterna(tablaExterna, campoId, campoExterno, key))
            {
                if (_repository.AgregarValorTablaExterna(tablaExterna, campoId, campoExterno, key, value))
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            else
            {
                return false;
            }
          
        }

        public Model.LoginAuth AuthenticateJSON(string username, string password)
        {

            string token = LCP.ODO.BusinessObjects.Usuario.GetLoginToken(username, password, "ED");
            Cardinal.EasyApps.ED.Model.LoginAuth loginAuth = new Cardinal.EasyApps.ED.Model.LoginAuth();
            if (!String.IsNullOrEmpty(token))
            {
                loginAuth.Result = "OK";
                loginAuth.Token = token;
            }
            else
            {
                loginAuth.Result = "FAIL";
                loginAuth.Token = string.Empty;
            }

            return loginAuth;
        }

        public Model.LoginAuth AuthenticateJSONLDAP(string username, string password)
        {
            string token = LCP.ODO.BusinessObjects.Usuario.GetLoginTokenIntegrateDomain(username, "ED");
            Cardinal.EasyApps.ED.Model.LoginAuth loginAuth = new Cardinal.EasyApps.ED.Model.LoginAuth();
            if (!String.IsNullOrEmpty(token))
            {
                loginAuth.Result = "OK";
                loginAuth.Token = token;
            }
            else
            {
                loginAuth.Result = "FAIL";
                loginAuth.Token = string.Empty;
            }

            return loginAuth;
        }

        public bool ChangePassword(LoginAuth token,string oldPassword, string newPassword)
        {
            if (!ValidoToken(token))
            {
                return false;
            }
            var usr = _repository.GetUsuarioNombre(token.Token);
            if (LCP.ODO.BusinessObjects.Usuario.ValidatePassword(usr, oldPassword,"ED"))
            {
                LCP.ODO.BusinessObjects.Usuario.CambiarPasswordUsuario(_repository.GetUsuarioId(token.Token), newPassword);
                _repository.ModificarDebeCambiarPassword(token.Token, false);
                return true;
            }
            else
            { 
                  return false;
            }
      

        }

        public bool EditValorTablaExterna(Model.LoginAuth token, string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {
            if (!ValidoToken(token))
            {
                return false;
            }


           
            if (_repository.ExisteKeyValorTablaExterna(tablaExterna, campoId, campoExterno, key))
            {
                if (_repository.EditarValorTablaExterna(tablaExterna, campoId, campoExterno, key, newValue))
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            else
            {
                return false;
            }
        }

        public List<Model.PlantillaCampo> GetCamposByPlantillaJSON(Model.LoginAuth token, string codPlantilla)
        {
            if (!ValidoToken(token))
            {
                return null;
            }

            return _repository.GetPlantillaCampos(codPlantilla).ToList();
        }

        public List<Model.Cliente> GetEmpresasJSON(Model.LoginAuth token)
        {
          
            if (!ValidoToken(token))
            {
                return null;
            }

            return _repository.GetEmpresas(token.Token).ToList();
        }
  
        private bool ValidoToken(LoginAuth token)
        {
            try
            {
                if (_repository.IsLoginTokenValid(token.Token))
                {

                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public List<Model.Plantilla> GetPlantillasByEmpresaJSON(Model.LoginAuth token, string codEmpresa)
        {
            if (!ValidoToken(token))
            {
                return null;
            }

            return _repository.GetPlantillas(token.Token, codEmpresa).ToList();
        }

        public List<Model.TablaExterna> GetTablasExternasJSON(Model.LoginAuth token)
        {
            if (!ValidoToken(token))
            {
                return null;
            }

            return _repository.GetTablasExternas(token.Token).ToList();
        }





        public string GetCampoDescripcion(string plantillaId, string campoId, string campoValue)
        {
            return _repository.GetCampoDescripcion(plantillaId, campoId, campoValue);
        }
    }
}