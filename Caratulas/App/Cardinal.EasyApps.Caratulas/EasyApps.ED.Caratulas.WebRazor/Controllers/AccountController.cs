﻿
using System.Web.Mvc;

using System.Web.Security;
using Cardinal.Caratulas.Models;


using Cardinal.EasyApps.ED.Model;
using Cardinal.EasyApps.ED.Ldap;
using MvcMiniProfiler;
using Cardinal.EasyApps.ED.Common.Dal;
using System;
using System.Configuration;

namespace Cardinal.Caratulas.Controllers
{
    public class AccountController : Controller
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;
        Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas _servicio;
        public AccountController(IRepository repository, Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas servicio)
        {
            _repository = repository;
            _servicio = servicio;
            _servicio.Repository = _repository;
            

        }
       
        public ActionResult LogOn()
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Get"))
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Post"))
            {
                if (ModelState.IsValid)
                {                    
                    using (profiler.Step("Autenticate JSON"))
                    {
                        LoginAuth loginAuth = new LoginAuth();
                        string ldapSrv = ConfigurationManager.AppSettings["LDAP_Server"];

                        if (!String.IsNullOrEmpty(ldapSrv))
                        {
                            if (AutenticacionLDAP.AutenticarUsuario(ldapSrv, model.UserName, model.Password))
                            {
                                loginAuth = _servicio.AuthenticateJSONLDAP(model.UserName, model.Password);
                            }
                        }
                        else
                        {
                            loginAuth = _servicio.AuthenticateJSON(model.UserName, model.Password);
                        }

                        if (loginAuth.Result == "OK")
                        {
                            using (profiler.Step("Autenticate OK"))
                            {
                                #region SaveTokenSession
                                Session["TokenTicket"] = loginAuth;
                                Session["UserName"] = model.UserName;
                                #endregion
                                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                {
                                    return Redirect(returnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Home");
                                }
                            }
                        }
                        else
                        {
                            using (profiler.Step("Autenticate FAIL"))
                            {
                                ModelState.AddModelError("", "El nombre de usuaro o contraseña es incorrecta.");
                            }
                        }
                    }


                }

                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("LogOn", "Account");
        }


        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    Csla.ApplicationContext.ClientContext["token"] = ((LoginAuth)Session["TokenTicket"]).Token; 
                   changePasswordSucceeded =  _servicio.ChangePassword((LoginAuth)Session["TokenTicket"], model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "La Password actual es incorrecta o la nueva es invalida.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

     
    }
}
