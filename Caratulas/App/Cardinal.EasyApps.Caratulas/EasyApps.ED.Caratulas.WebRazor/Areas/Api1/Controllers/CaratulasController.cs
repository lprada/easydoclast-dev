﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cardinal.EasyApps.ED.Common.Dal;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.Caratulas.WebRazor.Areas.Api1.Controllers
{
    public class CaratulasController : Controller
    {
        IRepository _repository;

        public CaratulasController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public ActionResult Caratulas(string loginToken)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratulas().ToList()
            };
        }

        [HttpPost]
        public ActionResult Caratula(string loginToken, long numeroCaratula)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratula(numeroCaratula)
            };
        }

        [HttpPost]
        public ActionResult Inbound(string loginToken, int dias)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratulas().Where(p => p.FechaInbound >= DateTime.Now.AddDays(-30)).ToList()
            };
        }

        [HttpPost]
        public ActionResult OutBound(string loginToken, int dias)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratulas().Where(p => p.FechaOutbound >= DateTime.Now.AddDays(-30)).ToList()
            };
        }

        [HttpPost]
        public ActionResult RealizarInbound(string loginToken, long numeroCaratula)
        {
            _repository.RealizarInbound(numeroCaratula);

            return new JsonResult
            {
                Data = string.Empty
            };
        }

        [HttpPost]
        public ActionResult RealizarOutbound(string loginToken, long numeroCaratula)
        {
            _repository.RealizarOutbound(numeroCaratula);

            return new JsonResult
            {
                Data = string.Empty
            };
        }

        [HttpPost]
        public ActionResult CaratulasByUser(string loginToken, string username)
        {
            return new JsonResult
            {
                Data = _repository.GetCaratulas(username).ToList()
            };
        }
    }
}