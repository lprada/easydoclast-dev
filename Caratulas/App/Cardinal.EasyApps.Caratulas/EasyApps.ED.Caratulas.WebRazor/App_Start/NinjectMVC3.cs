[assembly: WebActivator.PreApplicationStartMethod(typeof(EasyApps.ED.Caratulas.WebRazor.App_Start.NinjectMVC3), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(EasyApps.ED.Caratulas.WebRazor.App_Start.NinjectMVC3), "Stop")]

namespace EasyApps.ED.Caratulas.WebRazor.App_Start
{
    using System.Reflection;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Mvc;
    using Cardinal.EasyApps.ED.Common.Dal;
    using Cardinal.EasyApps.ED.Dal.EntityFramework;
    using System.Configuration;

    public static class NinjectMVC3 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestModule));
            DynamicModuleUtility.RegisterModule(typeof(HttpApplicationInitializationModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IRepository>()
                  .To<Repository>()
                  .InRequestScope()
                  .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString)

            ;

            string connectionMethod = ConfigurationManager.AppSettings["ConnectionMethod"];

            switch (connectionMethod)
            {
                case "Direct":
                    ConnectionDirect(kernel);
                    break;
                case "WebService":

                    ConnectionWebService(kernel);
                    break;
                default:
                    ConnectionDirect(kernel);
                    break;
            }
        }
  
        private static void ConnectionWebService(IKernel kernel)
        {
            kernel.Bind<Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas>()
                  .To<Cardinal.EasyApps.ED.Caratulas.WebRazor.Infraestructure.ConexionServiciosWebED>()
                  .InRequestScope()
                  .WithConstructorArgument("apiUrl", ConfigurationManager.AppSettings["ApiUrl"])
                  .WithConstructorArgument("apiVersion", ConfigurationManager.AppSettings["ApiVersion"])

            ;
        }
  
        private static void ConnectionDirect(IKernel kernel)
        {
          
            kernel.Bind<Cardinal.EasyApps.ED.Common.Servicios.IServicioCaratulas>()
                  .To<Cardinal.EasyApps.ED.Caratulas.WebRazor.Infraestructure.ConexionDirectaED>()
                  .InRequestScope()
                 // .WithConstructorArgument("repository", helper)
            ;
        }
    }
}
