﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace ED.Caratulas.Logistica.WPF.Reports
{
    public partial class InboundReportRadForm : Telerik.WinControls.UI.RadForm
    {
        public InboundReportRadForm(List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            InitializeComponent();
            Reports.InboundReport report = new InboundReport();
            report.DataSource = caratulas;
            this.reportViewer1.Report = report;
            this.reportViewer1.RefreshReport();

        }
    }
}
