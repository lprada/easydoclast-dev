﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace ED.Caratulas.Logistica.WPF.Reports
{
    public partial class OutboundReportRadForm : Telerik.WinControls.UI.RadForm
    {
        public OutboundReportRadForm(List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            InitializeComponent();
            Reports.OutboundReport report = new OutboundReport();
            report.DataSource = caratulas;
            this.reportViewer1.Report = report;
            this.reportViewer1.RefreshReport();

        }
    }
}
