﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace ED.Caratulas.Logistica.WPF
{
    public partial class RealizarOutboundRadForm : Telerik.WinControls.UI.RadForm
    {
        public RealizarOutboundRadForm()
        {
            InitializeComponent();
        }

        public string CodigoLote
        {
            get
            {
                return radMaskedEditBox1.Text.Replace("_","").Trim();
            }
        }

        private void RealizarOutboundRadForm_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                radMaskedEditBox1.Focus();
            }
        }

      
    }
}
