﻿namespace ED.Caratulas.Logistica.WPF
{
    partial class RealizarOutboundRadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RealizarOutboundRadForm));
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cancelarRadButton = new Telerik.WinControls.UI.RadButton();
            this.inboundRadButton = new Telerik.WinControls.UI.RadButton();
            this.radMaskedEditBox1 = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cancelarRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inboundRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radMaskedEditBox1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(616, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // radLabel1
            // 
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(171, 30);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Codigo de Lote";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.cancelarRadButton);
            this.flowLayoutPanel1.Controls.Add(this.inboundRadButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 52);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(610, 30);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // cancelarRadButton
            // 
            this.cancelarRadButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelarRadButton.Location = new System.Drawing.Point(477, 3);
            this.cancelarRadButton.Name = "cancelarRadButton";
            this.cancelarRadButton.Size = new System.Drawing.Size(130, 24);
            this.cancelarRadButton.TabIndex = 1;
            this.cancelarRadButton.Text = "Cancelar";
            this.cancelarRadButton.ThemeName = "TelerikMetro";
            // 
            // inboundRadButton
            // 
            this.inboundRadButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.inboundRadButton.Location = new System.Drawing.Point(341, 3);
            this.inboundRadButton.Name = "inboundRadButton";
            this.inboundRadButton.Size = new System.Drawing.Size(130, 24);
            this.inboundRadButton.TabIndex = 0;
            this.inboundRadButton.Text = "Realizar Outbound";
            this.inboundRadButton.ThemeName = "TelerikMetro";
            // 
            // radMaskedEditBox1
            // 
            this.radMaskedEditBox1.AllowPromptAsInput = false;
            this.radMaskedEditBox1.AutoSize = true;
            this.radMaskedEditBox1.Culture = new System.Globalization.CultureInfo("en-US");
            this.radMaskedEditBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radMaskedEditBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMaskedEditBox1.Location = new System.Drawing.Point(180, 3);
            this.radMaskedEditBox1.Mask = "AAAAAAAAAAAAAAAAAAAAAAAAA";
            this.radMaskedEditBox1.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.radMaskedEditBox1.Name = "radMaskedEditBox1";
            this.radMaskedEditBox1.Size = new System.Drawing.Size(433, 43);
            this.radMaskedEditBox1.TabIndex = 0;
            this.radMaskedEditBox1.TabStop = false;
            this.radMaskedEditBox1.Text = "_________________________";
            this.radMaskedEditBox1.ThemeName = "TelerikMetro";
            // 
            // RealizarOutboundRadForm
            // 
            this.AcceptButton = this.inboundRadButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelarRadButton;
            this.ClientSize = new System.Drawing.Size(616, 100);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RealizarOutboundRadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Realizar OutBound";
            this.ThemeName = "TelerikMetro";
            this.Load += new System.EventHandler(this.RealizarOutboundRadForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cancelarRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inboundRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMaskedEditBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Telerik.WinControls.UI.RadButton cancelarRadButton;
        private Telerik.WinControls.UI.RadButton inboundRadButton;
        private Telerik.WinControls.UI.RadMaskedEditBox radMaskedEditBox1;
    }
}
