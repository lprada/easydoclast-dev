﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace ED.Caratulas.Proc.WPF.Reports
{
    public partial class ProcessReportRadForm : Telerik.WinControls.UI.RadForm
    {
        public ProcessReportRadForm(List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            InitializeComponent();
            Reports.ProcessReport report = new ProcessReport();
            report.DataSource = caratulas;
            this.reportViewer1.Report = report;
            this.reportViewer1.RefreshReport();

        }
    }
}
