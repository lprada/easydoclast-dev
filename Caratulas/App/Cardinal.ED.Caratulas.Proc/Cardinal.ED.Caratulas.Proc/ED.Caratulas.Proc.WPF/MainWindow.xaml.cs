﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cardinal.EasyApps.ED.Api.Caratulas.Access;
using Telerik.Windows.Controls;
using System.ComponentModel;
using System.Configuration;
using Cardinal.EasyApps.ED.Common.Dal;
using EFRepository = Cardinal.EasyApps.ED.Dal.EntityFramework;

namespace ED.Caratulas.Proc.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        private Cardinal.EasyApps.ED.Model.LoginAuth _loginAuth;
        IRepository _repository;
        private string apiUrl = string.Empty;
        private string apiVersion = string.Empty;
        private string modoOffline = ConfigurationManager.AppSettings["modoOffline"];

        public MainWindow()
        {
            InitializeComponent();
            System.Reflection.Assembly assem = System.Reflection.Assembly.GetAssembly(typeof(MainWindow));
            System.Reflection.AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            string version = "Version {0}.{1}.{2}.{3}";
            version = System.String.Format(version,
                                           ver.Major,
                                           ver.Minor,
                                           ver.Build,
                                           ver.Revision);

            ribbon.ApplicationName = String.Format("EasyDoc ({0})", version);
            var appConfig = ConfigurationManager.AppSettings;

            apiUrl = appConfig["ApiUrl"];
            apiVersion = appConfig["ApiVersion"];
            _repository = new EFRepository.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
        }

        private void CerrarAplicacion()
        {
            Application.Current.Shutdown();
        }

        private void RadRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth authApi =
                new Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth(apiUrl, apiVersion);

                string token = string.Empty;
                int cantProcesos = 0;

                do
                {
                    Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm form = new Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm();

                    bool resDialog = form.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                    if (resDialog)
                    {
                        if (modoOffline == "true")
                        {
                            token = Guid.NewGuid().ToString();
                        }
                        else
                        {
                            token = authApi.GetLoginToken(form.UserName, form.Password);
                        }
                    }
                    else
                    {
                        token = string.Empty;
                        cantProcesos = 100;
                    }
                    cantProcesos++;
                    if (String.IsNullOrWhiteSpace(token) && resDialog)
                    {
                        MessageBox.Show("Ingreso denegado. Vuelva a intentar.");
                    }
                }
                while (String.IsNullOrWhiteSpace(token) && cantProcesos < 3);
                if (String.IsNullOrWhiteSpace(token))
                {
                    MessageBox.Show("Ingreso denegado. Cerrando Aplicacion");
                    CerrarAplicacion();
                }

                if (modoOffline == "true")
                {
                    Cardinal.EasyApps.ED.Model.LoginAuth loginAuth = new Cardinal.EasyApps.ED.Model.LoginAuth();
                    loginAuth.Token = token;
                    loginAuth.Result = "OK";
                    _loginAuth = loginAuth;
                }
                else
                {
                    _loginAuth = authApi.LoginToken;
                }

                RefreshDatos();
            }
        }

        private void RefreshDatos()
        {
            if (_loginAuth != null)
            {
                //Obtengo de template las sin procesar, luego para cada una busco la informacion de la caratula
                List<CaratulaPendienteProceso> caratulasSinProcesar = new List<CaratulaPendienteProceso>();
                var caratulas = _repository.GetNumeroCaratulasPendientesProcesar();

                Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
                new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);

                foreach (var item in caratulas)
                {
                    CaratulaPendienteProceso caratula = new CaratulaPendienteProceso();
                    var carRemoto = caratulaApi.GetCaratula(item.Key);
                    caratula.codigo = carRemoto.codigo;
                    caratula.codigo_3d = carRemoto.codigo_3d;
                    caratula.DetalleCampos = carRemoto.DetalleCampos;
                    caratula.empresa = carRemoto.empresa;
                    caratula.fecha = carRemoto.fecha;
                    caratula.FechaInbound = carRemoto.FechaInbound;
                    caratula.FechaOutbound = carRemoto.FechaOutbound;
                    caratula.nombre_empresa = carRemoto.nombre_empresa;
                    caratula.plantilla = carRemoto.plantilla;
                    caratula.usuario = carRemoto.usuario;

                    //Datos del Lote
                    caratula.Lote = item.Value.CodigoLote;
                    caratula.FechaCreacion = item.Value.FechaCreacion.Value;

                    caratulasSinProcesar.Add(caratula);
                }

                detalleRadDataPager.Source = caratulasSinProcesar;
                detalleGrid.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            RealizarProcessRadForm form = new RealizarProcessRadForm();

            if (detalleRadGridView.SelectedItems.Count() != 0)
            {
                var caratula = (CaratulaPendienteProceso)detalleRadGridView.SelectedCells[0].Item;
                form.CodLote = caratula.Lote;

                List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas = new List<Cardinal.EasyApps.ED.Model.CaratulaCS>();
                Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
                new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);
                DateTime fechaProceso = DateTime.Now;
                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ProcesarLote(form, caratulaApi, fechaProceso, caratulas);

                    //Poner reporte de Outbound
                    Reports.ProcessReportRadForm formReport = new Reports.ProcessReportRadForm(caratulas);
                    formReport.ShowDialog();
                    form.Close();
                    //form = new RealizarProcessRadForm();
                }

                RefreshDatos();
                
            }
        }
  
        private void ProcesarLote(RealizarProcessRadForm form, Caratula caratulaApi, DateTime fechaProceso, List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            var items = _repository.GetCaratulasPorLote(form.CodigoLote);

            foreach (var item in items)
            {
                Cardinal.EasyApps.ED.Model.CaratulaCS car;

                if (modoOffline == "true")
                {
                    car = _repository.GetCaratula(item.codigo);
                }
                else
                {
                    car = caratulaApi.GetCaratula(item.codigo.ToString());
                }

                if (car != null)
                {
                    //Mover a Lote Nuevo en Nuevo Template
                    //Creo un lote, mismo nombre mas _NROCAR
                    GenerarLoteCaratula(form.CodigoLote, car);


                    //Realizar Merge de Datos

                    car.FechaProceso = fechaProceso;
                    caratulas.Add(car);
                }
                else
                {
                    MessageBox.Show("No existe información para esa caratula.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
          
        }
  
        private void GenerarLoteCaratula(string codLote,Cardinal.EasyApps.ED.Model.CaratulaCS car)
        {
            //Mover a Lote Nuevo en Nuevo Template
            //Creo un lote, mismo nombre mas _NROCAR
            string loteCaratula = string.Format("{0}_CAR{1:00000000}", codLote, car.codigo);

            //mount
            var imagenesCaratula = _repository.GetImagenesCaratula(codLote,car.codigo).ToList();


            //Cargo los datos la caratula


            //se supone que todas las imagenes estan en el mismo lote
            string dirLoteCaratula = System.IO.Path.Combine(imagenesCaratula[0].PathRedImagen, loteCaratula);
            bool exists = System.IO.Directory.Exists(@dirLoteCaratula);

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(@dirLoteCaratula);
            }
            else
            {
                System.IO.Directory.Delete(@dirLoteCaratula,true);
                System.IO.Directory.CreateDirectory(@dirLoteCaratula);
            	
            }

            foreach (var img in imagenesCaratula)
            {

                string dirDest = System.IO.Path.Combine(dirLoteCaratula, System.IO.Path.GetFileName(img.PathRedImagenFull));
                System.IO.File.Copy(@img.PathRedImagenFull, dirDest,true);
            }


            //Crear Lote nuevo
            Cardinal.EasyApps.ED.Model.Lote lote = new Cardinal.EasyApps.ED.Model.Lote();

            lote.CantidadImagenes = imagenesCaratula.Count();
            lote.CodigoLote = loteCaratula;
            lote.FechaCreacion = DateTime.Now;
            lote.Plantilla = new Cardinal.EasyApps.ED.Model.Plantilla
            {
                CodigoPlantilla = car.CodigoPlantilla
            };
            lote.Tag = string.Format("Lote de Caratula: {0}", car.codigo);
            _repository.AddLote(lote, imagenesCaratula, car);
          
        }

        private void RadRibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            RefreshDatos();
        }

        private void RadRibbonButton_Click_3(object sender, RoutedEventArgs e)
        {
            RealizarProcessRadForm form = new RealizarProcessRadForm();
            List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas = new List<Cardinal.EasyApps.ED.Model.CaratulaCS>();
            Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
            new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);
            DateTime fechaProceso = DateTime.Now;
            while (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
              ProcesarLoteMerge(form, caratulaApi, caratulas);

                form = new RealizarProcessRadForm();
            }
            //Poner reporte de Outbound
            Reports.ProcessReportRadForm formReport = new Reports.ProcessReportRadForm(caratulas);
            formReport.ShowDialog();
            RefreshDatos();
        }

        private void ProcesarLoteMerge(RealizarProcessRadForm form, Caratula caratulaApi, List<Cardinal.EasyApps.ED.Model.CaratulaCS> caratulas)
        {
            var items = _repository.GetCaratulasProcesadasPorLote(form.CodigoLote);

            foreach (var item in items)
            {
                var car = caratulaApi.GetCaratula(item.codigo.ToString());
                _repository.MergeLote(form.CodigoLote, car);
                caratulas.Add(car);
            }

        }

        private void btnImportCarat_Click(object sender, RoutedEventArgs e)
        {
            ImportarArchivoRadForm form = new ImportarArchivoRadForm();

            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //borro caratulas locales
                _repository.BorrarCaratulas();

                //importo caratulas de archivo
                System.IO.StreamReader fs = new System.IO.StreamReader(form.PathArchivo);
                string linea;
                while ((linea = fs.ReadLine()) != null)
                {
                    Cardinal.EasyApps.ED.Model.CaratulaCS car = Newtonsoft.Json.JsonConvert.DeserializeObject<Cardinal.EasyApps.ED.Model.CaratulaCS>(linea);

                    if (car != null)
                    {
                        _repository.SaveCaratula(car);
                    }
                }
                
                form.Close();

                MessageBox.Show("Importación terminada");
            }
        }
  
     
    }
}